/*
Output based on s14o1
Using whether or not have common auditors
*/

local ver = "s14o1"
local folder = "cs_common_auditor"

local csvars = "COMM_AUDITOR_NAT_DUM COMM_AUDITOR_NAT_DUMMISS0"
local csvars_n : word count `csvars'

local ivs  = "S_RDQ_MOSTRE_LM_D S_RDQ_MOSTRE_LM_LOG1 S_RDQ_MOSTRE_LM_LOG1_R"
local ivs_n : word count `ivs'

local count 0

forvalues i=1/`csvars_n'{

   local csvars_i : word `i' of `csvars'

   forvalues j=1/`ivs_n'{

      local ivs_j : word `j' of `ivs'

      local diff_s = ""
      local chi2_s = ""

      qui: eststo clear

      local count_str = "000`count'"
      local len_e : length local count_str
      local len_s = `len_e' - 2
      local count_str = substr("`count_str'", `len_s', 3)

      import sas using "${rwp1_data_loc}/`ver'd1.sas7bdat", case(upper) clear

      local dvs="ABS_JONES_ATMCL1 ABS_MJONES_ATMCL1"
      local dvs_n : word count `dvs'
      local ctl="NATIONAL_TENURE SIZE_LOG LEV_DEMCL1 ROA_DEMCL1 ROA_DEMCL1_L1 LOSS CFO_DEMCL1 BM ALTMANZ SALEG_NDCL1_DEL1 LOG_CFOVM1_L3C RESTRUCTURING MA PAYT_DEMCL1_L1 RECT_DEMCL1_L1"

      keep if `csvars_i' != .
      capture drop G
      egen G = group(GVKEY_CGVKEY)

      forvalues k=1/`dvs_n'{
         local dvs_k : word `k' of `dvs'

         qui: reghdfe `dvs_k'  ///
           `ivs_j' ///
           `ctl' ///
           if `csvars_i' == 0 ///
           , absorb(GVKEY_CGVKEY YEAR) ///
           cluster(GVKEY_CGVKEY)
         qui: eststo
         qui: estadd scalar r2ap=e(r2_a)
         qui: estadd local fe "Yes"
         qui: estadd local cluster "Yes"
         qui: scalar b1  = _b[`ivs_j']
         qui: scalar se1 = _se[`ivs_j']
         qui: summarize `dvs_k' if `csvars_i' == 0
         qui: estadd scalar nobs = r(N)

         qui: reghdfe `dvs_k'  ///
           `ivs_j' ///
           `ctl' ///
           if `csvars_i' == 1 ///
           , absorb(GVKEY_CGVKEY YEAR) ///
           cluster(GVKEY_CGVKEY)
         qui: eststo
         qui: estadd scalar r2ap=e(r2_a)
         qui: estadd local fe "Yes"
         qui: estadd local cluster "Yes"
         qui: scalar b2  = _b[`ivs_j']
         qui: scalar se2 = _se[`ivs_j']
         qui: summarize `dvs_k' if `csvars_i' == 1
         qui: estadd scalar nobs = r(N)

         qui: scalar diff = scalar(b1) - scalar(b2)
         qui: scalar chi2 = scalar(diff)^2 / (scalar(se1)^2 + scalar(se2)^2)
         qui: scalar p    = 1 - chi2(1, scalar(chi2))
         local diff : di %8.3f scalar(diff)
         local chi2 : di %8.3f scalar(chi2)
         if scalar(p) < 0.01 local diff = "`diff'" + "$^{***}$"
         else if scalar(p) < 0.05 local diff = "`diff'" + "$^{**}$"
         else if scalar(p) < 0.1 local diff = "`diff'" + "$^{*}$"
         local diff_s = "`diff_s'" + "& \multicolumn{2}{S}{`diff'}"
         local chi2_s = "`chi2_s'" + "& \multicolumn{2}{S}{`chi2'}"
         }

      import sas using "${rwp1_data_loc}/`ver'd2.sas7bdat", case(upper) clear

      local dvs="RESTATS_DUMMY"
      local dvs_n : word count `dvs'
      local ctl="NATIONAL_TENURE SIZE_LOG LEV_DEMCL1 ROA_DEMCL1 ROA_DEMCL1_L1 LOSS CFO_DEMCL1 BM ALTMANZ SALEG_NDCL1_DEL1 LOG_CFOVM1_L3C RESTRUCTURING MA PAYT_DEMCL1_L1 RECT_DEMCL1_L1"

      keep if `csvars_i' != .
      capture drop G
      egen G = group(GVKEY_CGVKEY)

      forvalues k=1/`dvs_n'{
         local dvs_k : word `k' of `dvs'
         qui: clogit `dvs_k' ///
           `ivs_j' ///
           `ctl' i.FYEAR ///
           , group(GVKEY_CGVKEY)
         qui: predict phat if e(sample)

         qui: logit `dvs_k' ///
           `ivs_j' ///
           `ctl' i.FYEAR ///
           i.G  ///
           if phat !=. & `csvars_i' == 0 ///
           , cluster(GVKEY_CGVKEY)
         qui: eststo
         qui: estadd scalar r2ap=e(r2_p)
         qui: estadd local fe "Yes"
         qui: estadd local cluster "Yes"
         qui: scalar b1  = _b[`ivs_j']
         qui: scalar se1 = _se[`ivs_j']
         qui: summarize `dvs_k' if `csvars_i' == 0
         qui: estadd scalar nobs = r(N)

         qui: logit `dvs_k' ///
           `ivs_j' ///
           `ctl' i.FYEAR ///
           i.G  ///
           if phat!=. & `csvars_i' == 1 ///
           , cluster(GVKEY_CGVKEY)
         qui: eststo
         qui: estadd scalar r2ap=e(r2_p)
         qui: estadd local fe "Yes"
         qui: estadd local cluster "Yes"
         qui: scalar b2  = _b[`ivs_j']
         qui: scalar se2 = _se[`ivs_j']
         qui: summarize `dvs_k' if `csvars_i' == 1
         qui: estadd scalar nobs = r(N)

         qui: scalar diff = scalar(b1) - scalar(b2)
         qui: scalar chi2 = scalar(diff)^2 / (scalar(se1)^2 + scalar(se2)^2)
         qui: scalar p    = 1 - chi2(1, scalar(chi2))
         local diff : di %8.3f scalar(diff)
         local chi2 : di %8.3f scalar(chi2)
         if scalar(p) < 0.01 local diff = "`diff'" + "$^{***}$"
         else if scalar(p) < 0.05 local diff = "`diff'" + "$^{**}$"
         else if scalar(p) < 0.1 local diff = "`diff'" + "$^{*}$"
         local diff_s = "`diff_s'" + "& \multicolumn{2}{S}{`diff'}"
         local chi2_s = "`chi2_s'" + "& \multicolumn{2}{S}{`chi2'}"
         }

      import sas using "${rwp1_data_loc}/`ver'd4.sas7bdat", case(upper) clear

      local dvs="MEET_OR_BEAT_MEDIAN"
      local dvs_n : word count `dvs'
      local ctl="NATIONAL_TENURE SIZE_LOG LEV_DEMCL1 ROA_DEMCL1 ROA_DEMCL1_L1 LOSS CFO_DEMCL1 BM ALTMANZ SALEG_NDCL1_DEL1 LOG_CFOVM1_L3C RESTRUCTURING MA PAYT_DEMCL1_L1 RECT_DEMCL1_L1 NUMEST_LOG1 FCST_DISP"

      keep if `csvars_i' != .
      capture drop G
      egen G = group(GVKEY_CGVKEY)

      forvalues k=1/`dvs_n'{
         local dvs_k : word `k' of `dvs'
         qui: clogit `dvs_k' ///
           `ivs_j' ///
           `ctl' i.FYEAR ///
           , group(GVKEY_CGVKEY)
         qui: predict phat if e(sample)

         qui: logit `dvs_k'  ///
           `ivs_j' ///
           `ctl' i.FYEAR ///
           i.G if phat!=. & `csvars_i' == 0 ///
           , cluster(GVKEY_CGVKEY)
         qui: eststo
         qui: estadd scalar r2ap=e(r2_p)
         qui: estadd local fe "Yes"
         qui: estadd local cluster "Yes"
         qui: scalar b1  = _b[`ivs_j']
         qui: scalar se1 = _se[`ivs_j']
         qui: summarize `dvs_k' if `csvars_i' == 0
         qui: estadd scalar nobs = r(N)

         qui: logit `dvs_k'  ///
           `ivs_j' ///
           `ctl' i.FYEAR ///
           i.G if phat!=. & `csvars_i' == 1 ///
           , cluster(GVKEY_CGVKEY)
         qui: eststo
         qui: estadd scalar r2ap=e(r2_p)
         qui: estadd local fe "Yes"
         qui: estadd local cluster "Yes"
         qui: scalar b2  = _b[`ivs_j']
         qui: scalar se2 = _se[`ivs_j']
         qui: summarize `dvs_k' if `csvars_i' == 1
         qui: estadd scalar nobs = r(N)

         qui: scalar diff = scalar(b1) - scalar(b2)
         qui: scalar chi2 = scalar(diff)^2 / (scalar(se1)^2 + scalar(se2)^2)
         qui: scalar p    = 1 - chi2(1, scalar(chi2))
         local diff : di %8.3f scalar(diff)
         local chi2 : di %8.3f scalar(chi2)
         if scalar(p) < 0.01 local diff = "`diff'" + "$^{***}$"
         else if scalar(p) < 0.05 local diff = "`diff'" + "$^{**}$"
         else if scalar(p) < 0.1 local diff = "`diff'" + "$^{*}$"
         local diff_s = "`diff_s'" + "& \multicolumn{2}{S}{`diff'}"
         local chi2_s = "`chi2_s'" + "& \multicolumn{2}{S}{`chi2'}"
         }

      esttab using  ///
        $rwp1_folder_loc/`folder'/reg_table_`count_str'.tex ///
        , booktabs unstack nobaselevels replace ///
        prehead( ///
        "\clearpage" ///
        "{" ///
        "\sisetup{" ///
        "table-space-text-pre={(}," ///
        "table-space-text-post={$^{***}$}," ///
        "}" ///
        "\begin{landscape}" ///
        "\begin{center}" ///
        "\begin{longtable} " ///
        "{>{\kern-\tabcolsep" ///
        "\raggedright\arraybackslash}l" ///
        "* {@M} {S[table-format=-3.3, " ///
        "table-column-width=.75in]}" ///
        "<{\kern-\tabcolsep}}" ///
        "\caption{Cross-sectional Tests} \\" ///
        "\multicolumn{@span}" ///
        "{>{\kern-\tabcolsep}l<{\kern-\tabcolsep}}" ///
        "{Based on `csvars_i'} \\" ///
        "\toprule" ///
        "& \multicolumn{2}{c}{ABS_JONES_ATMCL1}" ///
        "& \multicolumn{2}{c}{ABS_MJONES_ATMCL1}" ///
        "& \multicolumn{2}{c}{RESTATS_DUMMY}" ///
        "& \multicolumn{2}{c}{MEET_OR_BEAT_MEDIAN} \\" ///
        "\cmidrule(lr){2-3} \cmidrule(lr){4-5}" ///
        "\cmidrule(lr){6-7} \cmidrule(lr){8-9}" ///
        "& {Not Common} & {Common} " ///
        "& {Not Common} & {Common} " ///
        "& {Not Common} & {Common} " ///
        "& {Not Common} & {Common} " ///
        "\\ \midrule" ///
        "\endfirsthead" ///
        "\multicolumn{@span}" ///
        "{>{\kern-\tabcolsep}c" ///
        "<{\kern-\tabcolsep}}" ///
        "{\textbf{\tablename\" ///
        "\thetable{}:} (Continued)} \\" ///
        "\midrule" ///
        "& \multicolumn{2}{c}{ABS_JONES_ATMCL1}" ///
        "& \multicolumn{2}{c}{ABS_MJONES_ATMCL1}" ///
        "& \multicolumn{2}{c}{RESTATS_DUMMY}" ///
        "& \multicolumn{2}{c}{MEET_OR_BEAT_MEDIAN} \\" ///
        "\cmidrule(lr){2-3} \cmidrule(lr){4-5}" ///
        "\cmidrule(lr){6-7} \cmidrule(lr){8-9}" ///
        "& {Not Common} & {Common} " ///
        "& {Not Common} & {Common} " ///
        "& {Not Common} & {Common} " ///
        "& {Not Common} & {Common} " ///
        "\\ \midrule" ///
        "\endhead" ///
        "\midrule" ///
        "\multicolumn{@span}" ///
        "{>{\kern-\tabcolsep}r" ///
        "<{\kern-\tabcolsep}}" ///
        "{(\textit{Continued on next page})} \\" ///
        "\endfoot" ///
        "\bottomrule" ///
        "\endlastfoot" ///
        )  ///
        posthead("") ///
        prefoot("\midrule")  ///
        postfoot( ///
        "\cmidrule(lr){2-3} \cmidrule(lr){4-5}" ///
        "\cmidrule(lr){6-7} \cmidrule(lr){8-9}" ///
        "Difference `diff_s' \\" ///
        "$\chi^2$ for Difference `chi2_s' \\" ///
        "\end{longtable}" ///
        "\end{center}" ///
        "{\setstretch{1} \footnotesize" ///
        "\input{tabnote.tex}" ///
        "\par}" ///
        "\end{landscape}" ///
        "}" ///
        ) ///
        drop(*.G *.FYEAR) ///
        varlabels(_cons "\textit{Intercept}") ///
        nogaps b(3) t(3) ///
        eqlabels(none) collabels(none) nonumbers mlabels(none) ///
        stats(nobs r2 r2ap fe cluster, ///
        fmt(%9.0g %8.3f %8.3f %~12s %~12s) ///
        labels("Observations" "$ R^2 $" "Adj. / Pseudo $ R^2 $" "Include firm and year dummy" "Cluster by firm") ///
        layout("{@}" "{@}" "{@}" "{@}" "{@}") ///
        ) ///
        star(* 0.1 ** 0.05 *** 0.01)

      local count = `count' + 1

      }
   }

cd $rwp1_folder_loc/`folder'/
python script cs_common_auditor.py
cd `:environment USERPROFILE'/Documents
