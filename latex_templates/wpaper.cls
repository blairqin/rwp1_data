\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{wpaper}[2019/12/12 Informal Double-spaced LaTeX Working Paper Class]

\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}%
}
\DeclareOption{narrow}{%
  \AtEndOfPackage{\geometry{margin = 1.27cm}
	\resetHeadWidth%
  }%
}
\DeclareOption{moderate}{%
  \AtEndOfPackage{\geometry{
      top = 2.54 cm%
      bottom = 2.54cm%
      left = 1.91cm%
      right = 1.91cm%
    }
	\resetHeadWidth%
  }
}
\DeclareOption{wide}{%
  \AtEndOfPackage{\geometry{
      top = 2.54 cm%
      bottom = 2.54cm%
      left = 5.08cm%
      right = 5.08cm%
    }
	\resetHeadWidth%
  }
}
\DeclareOption{doublespacing}{%
  \AtEndOfPackage{%
    \setstretch{2}%
  }
}
\DeclareOption{singlespacing}{%
  \AtEndOfPackage{%
    \setstretch{1}%
  }
}
\DeclareOption{onehalfspacing}{%
  \AtEndOfPackage{%
    \setstretch{1.5}%
  }
}
\DeclareOption{sas}{%
  \AtEndOfPackage{%
    \input{listings_sas.tex}%
  }
}
\DeclareOption{stata}{%
  \AtEndOfPackage{%
    \input{listings_stata.tex}%
  }
}
\DeclareOption{times}{%
  \AtEndOfPackage{
    \setmainfont[
      BoldFont=texgyretermes-bold.otf
      , ItalicFont=texgyretermes-italic.otf
      , BoldItalicFont=texgyretermes-bolditalic.otf
    ]{texgyretermes-regular.otf}
    \setsansfont{texgyreheros-regular.otf}
    \setmonofont{texgyrecursor-regular.otf}
    \setmathfont{texgyretermes-math.otf}
  }
}
\DeclareOption{schola}{%
  \AtEndOfPackage{
    \setmainfont[
      BoldFont=texgyreschola-bold.otf
      , ItalicFont=texgyreschola-italic.otf
      , BoldItalicFont=texgyreschola-bolditalic.otf
    ]{texgyreschola-regular.otf}
    \setsansfont{texgyreheros-regular.otf}
    \setmonofont{texgyrecursor-regular.otf}
    \setmathfont{texgyreschola-math.otf}
  }
}
\ProcessOptions\relax

\LoadClass{article}

% Amsmath should be loaded before unicode-math
\RequirePackage{amsmath}

% Select Font and Unicode unicode-math
\RequirePackage{fontspec, unicode-math}

% Page Margin
\RequirePackage{geometry}%
\geometry{margin = 2.54cm}%

% Line Space
\RequirePackage{setspace}%
\RequirePackage{etoolbox, apptools}%
\setstretch{2}
\AtBeginEnvironment{tabular}{\setstretch{1}\small}
\AtBeginEnvironment{longtable}{\setstretch{1}\small}
\AtBeginEnvironment{tabularx}{\setstretch{1}\small}
\AtBeginEnvironment{tablenotes}{\setstretch{1}\footnotesize}
\AtBeginEnvironment{TableNotes}{\setstretch{1}\footnotesize}
\AtBeginEnvironment{tabularx}{\setstretch{1}\small}
\AtBeginEnvironment{tabu}{\setstretch{1}\small}
\AtBeginEnvironment{longtabu}{\setstretch{1}\small}
\AtBeginEnvironment{equation}{\setstretch{1}}
\AtBeginEnvironment{align}{\setstretch{1}}
\AtBeginEnvironment{equation*}{\setstretch{1}}
\AtBeginEnvironment{align*}{\setstretch{1}}
\AtBeginEnvironment{tabule}{\centering}
\AtBeginEnvironment{figure}{\centering\small}
\AtBeginEnvironment{lstlisting}{\setstretch{1}}
\AtBeginEnvironment{gather}{\setstretch{1}}
\AtBeginEnvironment{gather*}{\setstretch{1}}
\AtBeginEnvironment{appendices}{\appendixtrue}

% Hyperref
\RequirePackage{hyperref}
\hypersetup{
  colorlinks=true
  , citecolor=blue
  , linkcolor=black
  , urlcolor=blue
}
\urlstyle{same}

% References
\RequirePackage[english]{babel}%
\RequirePackage{csquotes,xpatch}%
\RequirePackage[
backend = biber
, authordate
, citetracker=true
, sortcites=true
, sorting=nyt
, sortlocale=en_US
, pagetracker=true
, minnames=1
, maxcitenames=1
, natbib
, includeall=false
, doi=true
, url=true
, giveninits=true
, uniquename=false
]{biblatex-chicago}

\AtEveryBibitem{
  \clearfield{month}
  \clearfield{day}
  \clearfield{urlyear}
  \clearfield{urlmonth}
  \clearfield{urlday}
  \clearfield{note}
}

% Reformat Cite
\DeclareFieldFormat{citehyperref}{%
  \DeclareFieldAlias{bibhyperref}{noformat}% Avoid nested links
  \bibhyperref{#1}}

\DeclareFieldFormat{textcitehyperref}{%
  \DeclareFieldAlias{bibhyperref}{noformat}% Avoid nested links
  \bibhyperref{%
    #1%
    \ifbool{cbx:parens}
    {\bibcloseparen\global\boolfalse{cbx:parens}}
    {}}}

\savebibmacro{cite}
\savebibmacro{textcite}

\renewbibmacro*{cite}{%
  \printtext[citehyperref]{%
    \restorebibmacro{cite}%
    \usebibmacro{cite}}}

\renewbibmacro*{textcite}{%
  \ifboolexpr{
    ( not test {\iffieldundef{prenote}} and
    test {\ifnumequal{\value{citecount}}{1}} )
    or
    ( not test {\iffieldundef{postnote}} and
    test {\ifnumequal{\value{citecount}}{\value{citetotal}}} )
  }
  {\DeclareFieldAlias{textcitehyperref}{noformat}}
  {}%
  \printtext[textcitehyperref]{%
    \restorebibmacro{textcite}%
    \usebibmacro{textcite}}}

% Reformat Title
\DeclareFieldFormat[article]{title}{#1}
\DeclareFieldFormat[inproceedings]{title}{\mkbibemph{#1}}
\DeclareFieldFormat[inproceedings]{booktitle}{Proceedings of #1}
\AtEveryCitekey{\ifciteseen{}{\defcounter{maxnames}{5}}}

% Url and doi
\DeclareFieldFormat{doi}{%
  {\url{https://doi.org/#1}}
}
\DeclareFieldFormat{url}{%
  Available at\addcolon\space\url{#1}
}
\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map{
      \step[fieldsource=doi,final]
      \step[fieldset=url,null]
    }
  }
}

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.0pt}
\makeatletter
  \ifcsname f@nch@setoffs\endcsname\else%
  \let\f@nch@setoffs\fancy@setoffs
\fi
\makeatother
\makeatletter
\newcommand{\resetHeadWidth}{\f@nch@setoffs}
\makeatother

% Title Format
\RequirePackage[explicit]{titlesec}
\titleformat{\section}{%
  \normalsize \bfseries \filcenter}{%
  \IfAppendix{\appendixname~}{}%
  \IfAppendix{\Alph{section}}{\Roman{section}}%
  \IfAppendix{:}{.}}{%
  0.5em}{%
  \MakeUppercase{#1}}
\titlespacing{\section}{0pt}{*1}{*1}
\titleformat{\subsection}{%
  \normalsize \bfseries \filright}{}{0em}{#1}
\titlespacing{\subsection}{0pt}{*1}{*1}
\titleformat{\subsubsection}{%
\normalsize \bfseries \itshape \filright}{}{0em}{#1}
\titlespacing{\subsubsection}{0pt}{*1}{*1}
\titleformat{\paragraph}{%
  \normalsize \bfseries \filright}{}{0em}{%
  \indent #1}
\titlespacing{\paragraph}{0pt}{*1}{*1}

% Table
\RequirePackage{tabularx, longtable, booktabs, siunitx}
\RequirePackage{threeparttable, threeparttablex}
\RequirePackage{ltablex}

% Float
\usepackage{float}
%\floatsetup[table]{capposition=top}
%\floatsetup[figure]{capposition=top}
\RequirePackage{caption, subcaption}
\captionsetup{font=small
  , labelfont=bf
  , justification=centering
  , position=auto
  , singlelinecheck=false
}
\captionsetup[subtable]{labelsep=colon
  , justification=raggedright
  , labelformat=simple
}

\renewcommand{\thesubtable}{Panel \Alph{subtable}}

% Appendixes
\RequirePackage[title]{appendix}

% Graphicx
\RequirePackage{graphicx}

% Color
\RequirePackage[svgnames, table]{xcolor}
\RequirePackage{color}

% Landscape Page
\RequirePackage{pdflscape}