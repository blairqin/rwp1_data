from rwp1_data.output_functions import parse_tab, add_header_footer, add_row_color, reformat_header
from rwp1_data import loc
import pandas as pd
import os
import sys
import json

if __name__ == '__main__':
    pd.options.display.max_colwidth = 1000
    table_files = []
    # Parse all regression tables
    for reg_table_file in sorted(os.listdir()):
        if reg_table_file.startswith('reg_table') & reg_table_file.endswith('.tex'):
            t = parse_tab(file=reg_table_file)
            with open(reg_table_file, 'w') as f:
                f.write(t[0])
            table_files.append(reg_table_file)
    s = r"""
    \documentclass[singlespacing, times]{wpaper}
    \addbibresource{../rwp1.bib}
    \sisetup{
        detect-all,
        table-alignment=center,
        table-text-alignment=center,
        input-signs= + -,
        input-ignore={,},
        input-decimal-markers={.},
        group-separator={,},
        group-minimum-digits=4,
        table-align-text-pre=false,
        input-open-uncertainty=,
        input-close-uncertainty=,
        table-align-text-post=false
    }
    \begin{document}
    """
    for table_file in table_files:
        s = s + r'''
        \input{{{0}}}
        '''.format(table_file)
    s = s + r'''
    \clearpage
    {\setstretch{1}
    \printbibliography}
    \end{document}'''
    file_name = os.path.basename(sys.argv[0]).split('.')[0] + '.tex'
    with open(file_name, 'w') as f:
        f.write(s)
    os.system('latexmk -pdf -lualatex {0}'.format(file_name))
    os.system('latexmk -c {0}'.format(file_name))
