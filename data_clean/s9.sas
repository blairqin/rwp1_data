/*
Based on s8
Add auditor search of client's customers' RECENT financial reports
i.e., the gap between search date and file date is less than 360 days
*/

data _tmp_rwp1_data;
    set rwp1.s8;
run;

data _tmp_auditor_search_10kq;
    set elog.auditor_search_nocrawler_ftd(
        where=(
        (find(form_type, '10-K', 'i')>0
        or find(form_type, '10K', 'i')>0
        or find(form_type, '10-Q', 'i')>0
        or find(form_type, '10Q', 'i')>0)
        and (auditor_id not in (
        13, 18, 20, 27, 29, 34, 40, 41
        ))
        ));
    all_search = sum(of htm xml txt xbrl other);
run;

%include "./rwp1_data/match_search_mostrc.sas";

%macro gen_log(
    dsetin =
    , var  =
    );

    %local i var_i;

    data &dsetin;
        set &dsetin;
        if cmiss(of &var)=0 then do;
            &var._d    = 1;
            &var._log1 = log(1 + &var);
            end;
        else if 2003<=fyear<=2016 then do;
            &var       = 0;
            &var._d    = 0;
            &var._log1 = 0;
            end;
        else call missing(of &var._d &var._log1);
    run;
    %mend gen_log;

/* Auditor info search from -365 to filing_date */

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_10kq
    , search_vars       = all_search
    , new_name          = s_fdate_mostre
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_fdate_mostre
    );

/* Auditor info search from -365 to rdq */

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_10kq
    , search_vars       = all_search
    , new_name          = s_rdq_mostre
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_rdq_mostre
    );

/* Auditor search without robots using lm method */

data _tmp_auditor_search_1;
    set _tmp_auditor_search_10kq;
    where lm=0;
run;

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = all_search
    , new_name          = s_fdate_mostre_lm
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_fdate_mostre_lm
    );

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = all_search
    , new_name          = s_rdq_mostre_lm
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_rdq_mostre_lm
    );

/* Auditor search without robots using drt method */

data _tmp_auditor_search_1;
    set _tmp_auditor_search_10kq;
    where drt=0;
run;

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = all_search
    , new_name          = s_fdate_mostre_drt
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_fdate_mostre_drt
    );

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = all_search
    , new_name          = s_rdq_mostre_drt
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_rdq_mostre_drt
    );

/* Auditor search without robots using rpv method */

data _tmp_auditor_search_1;
    set _tmp_auditor_search_10kq;
    where rpv=0;
run;

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = all_search
    , new_name          = s_fdate_mostre_rpv
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_fdate_mostre_rpv
    );

%match_search_mostrc(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = all_search
    , new_name          = s_rdq_mostre_rpv
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , var  = s_rdq_mostre_rpv
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s9
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
