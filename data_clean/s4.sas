/*
Merge header related variables
e.g., sic, ffi48
*/

data _tmp_rwp1_data;
    set rwp1.stage3;
run;

/* Merge sic and ffi48 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_header
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = sic1 sic2 sic3 sic4
    ffi5 ffi10 ffi12 ffi17 ffi30 ffi38 ffi48 ffi49
    , new_names           =
);

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_header
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = sic1 sic2 sic3 sic4
    ffi5 ffi10 ffi12 ffi17 ffi30 ffi38 ffi48 ffi49
    , new_names           = csic1 csic2 csic3 csic4
    cffi5 cffi10 cffi12 cffi17 cffi30 cffi38 cffi48 cffi49
);

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.stage4
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
