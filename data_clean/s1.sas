* Stage 1;

data _tmp_rwp1_data;
    set wrdsapps.seglink;
    where salecs>0;
run;

* Accumulate data into gvkey, cgvkey, srcdate level;

%means(
    dsetin             = _tmp_rwp1_data
    , by               = gvkey scusip stic cgvkey ccusip ctic srcdate
    , allow_missing_by = N
    , variables        = salecs
    , method           = sum
    , new_names        = salecs
    , mean_out         = _tmp_rwp1_data
    );

%var_rename(
    dsetin      = _tmp_rwp1_data
    , old_names = scusip stic
    , new_names = cusip tic
    );

* Merge header information from comp.funda_header;
* Supplier side;

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_header
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = fyear fyr ibtic sic naics cik permno audit_op_key
    , new_names           =
    );

* Customer side, get the most recent header data;

proc sql undo_policy=none;
    create table _tmp_rwp1_data as
        select a.*
        , b.datadate as cdatadate
        , b.fyear as cfyear
        , b.fyr as cfyr
        , b.ibtic as cibtic
        , b.sic as csic
        , b.naics as cnaics
        , b.cik as ccik
        , b.permno as cpermno
        , b.audit_op_key as caudit_op_key
        from _tmp_rwp1_data a
        left join comp.funda_header b
        on a.cgvkey = b.gvkey
        and 0<= intck('month', a.srcdate, b.datadate, 'C')<=11
        group by a.cgvkey, a.srcdate
        having b.datadate = min(b.datadate)
        ;
quit;

/* Output Stag 1 data as rwp1.stage1 */
/* Note that fyear contains missing value */

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.stage1
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
