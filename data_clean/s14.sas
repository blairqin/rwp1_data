/*
Based on s13
Merge common vars and relationship-specifc investments variables
Variables are based on
Cho, Young Jun, Yongtae Kim, and Yoonseok Zang. 2020.
“Information Externalities and Voluntary Disclosure:
Evidence from a Major Customer’s Earnings Announcement.”
The Accounting Review, January, 0000–0000.
https://doi.org/10.2308/tar-2017-0129.
*/

data _tmp_rwp1_data;
    set rwp1.s13;
run;

/* Merge location related variables */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey
    , right_table         = comp.company_loc
    , right_table_index   = gvkey
    , allow_missing_index = N
    , variables           = address google_formatted_address
    zipcode_combined google_lat google_lng cbsa10
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey
    , right_table         = comp.company_loc
    , right_table_index   = gvkey
    , allow_missing_index = N
    , variables           = address google_formatted_address
    zipcode_combined google_lat google_lng cbsa10
    , new_names           = caddress cgoogle_formatted_address
    czipcode_combined cgoogle_lat cgoogle_lng ccbsa10
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if cmiss(of google_lat google_lng cgoogle_lat cgoogle_lng)=0
        then distance_miles = geodist(google_lat, google_lng
        , cgoogle_lat, cgoogle_lng, "M");
    else call missing(distance_miles);
    if cmiss(of distance_miles)=0 then do;
        dist_dumle100      = (distance_miles <= 100);
        dist_dumle100miss0 = (distance_miles <= 100);
        end;
    else do;
        call missing(dist_dumle100);
        dist_dumle100miss0 = 0;
        end;
    if cmiss(of cbsa10 ccbsa10)=0 then do;
        comm_cbsa_dum      = (cbsa10 = ccbsa10);
        comm_cbsa_dummiss0 = (cbsa10 = ccbsa10);
        end;
    else do;
        call missing(comm_cbsa_dum);
        comm_cbsa_dummiss0 = 0;
        end;
    if not missing(dist_dumle100)
        or not missing(comm_cbsa_dum) then do;
        comm_place_dum       = (
            dist_dumle100    = 1
            or comm_cbsa_dum = 1);
        comm_place_dummiss0  = (
            dist_dumle100    = 1
            or comm_cbsa_dum = 1);
        end;
    else do;
        call missing(comm_place_dum);
        comm_place_dummiss0 = 0;
        end;
    if cmiss(of auditor_fkey cauditor_fkey)=0 then do;
        comm_auditor_dum      = (auditor_fkey = cauditor_fkey);
        comm_auditor_dummiss0 = (auditor_fkey = cauditor_fkey);
        end;
    else do;
        call missing(comm_auditor_dum);
        comm_auditor_dummiss0 = 0;
        end;
    if cmiss(of auditor_fkey cauditor_fkey
        auditor_country cauditor_country)=0 then do;
        comm_auditor_nat_dum      = (
            auditor_fkey            = cauditor_fkey
            and auditor_country     = cauditor_country
            );
        comm_auditor_nat_dummiss0 = (
            auditor_fkey            = cauditor_fkey
            and auditor_country     = cauditor_country
            );
        end;
    else do;
        call missing(comm_auditor_nat_dum);
        comm_auditor_nat_dummiss0 = 0;
        end;
    if cmiss(of auditor_fkey cauditor_fkey
        auditor_country cauditor_country
        auditor_state cauditor_state
        auditor_city cauditor_city)=0 then do;
        comm_auditor_city_dum      = (
            auditor_fkey             = cauditor_fkey
            and auditor_country      = cauditor_country
            and auditor_state        = cauditor_state
            and auditor_city         = cauditor_city
            );
        comm_auditor_city_dummiss0 = (
            auditor_fkey             = cauditor_fkey
            and auditor_country      = cauditor_country
            and auditor_state        = cauditor_state
            and auditor_city         = cauditor_city
            );
        end;
    else do;
        call missing(comm_auditor_city_dum);
        comm_auditor_city_dummiss0 = 0;
        end;
run;

/* Merge relaionship-specific investments variables */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey cgvkey srcdate
    , right_table         = wrdsapps.seglink_rsi
    , right_table_index   = gvkey cgvkey srcdate
    , allow_missing_index = N
    , variables           = rsi1 rsi2 rsi3
    rsi_sic1 rsi_sic2 rsi_sic3 rsi_sic4
    rsi_ffi5 rsi_ffi10 rsi_ffi12 rsi_ffi17 rsi_ffi30
    rsi_ffi38 rsi_ffi48 rsi_ffi49
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_filtered
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = sale
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s14
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );

%check(_tmp_rwp1_data);
