/*
Merge additional mjones var
*/

data _tmp_rwp1_data;
    set rwp1.s10;
run;

/* Merge jll2014m2 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjones_jll2014m2
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_jll2014m2 mjones_jll2014m2
    abs_jones_jll2014m2 abs_mjones_jll2014m2
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjones_jll2014m2
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_jll2014m2 mjones_jll2014m2
    abs_jones_jll2014m2 abs_mjones_jll2014m2
    , new_names           = cjones_jll2014m2 cmjones_jll2014m2
    cabs_jones_jll2014m2 cabs_mjones_jll2014m2
    );

/* Merge jll2014m3 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjones_jll2014m3
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_jll2014m3 mjones_jll2014m3
    abs_jones_jll2014m3 abs_mjones_jll2014m3
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjones_jll2014m3
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_jll2014m3 mjones_jll2014m3
    abs_jones_jll2014m3 abs_mjones_jll2014m3
    , new_names           = cjones_jll2014m3 cmjones_jll2014m3
    cabs_jones_jll2014m3 cabs_mjones_jll2014m3
    );

/* Merge rw2010m2 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjones_rw2010m2
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_rw2010m2 mjones_rw2010m2
    abs_jones_rw2010m2 abs_mjones_rw2010m2
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjones_rw2010m2
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_rw2010m2 mjones_rw2010m2
    abs_jones_rw2010m2 abs_mjones_rw2010m2
    , new_names           = cjones_rw2010m2 cmjones_rw2010m2
    cabs_jones_rw2010m2 cabs_mjones_rw2010m2
    );

/* Merge rw2010m3 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjones_rw2010m3
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_rw2010m3 mjones_rw2010m3
    abs_jones_rw2010m3 abs_mjones_rw2010m3
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjones_rw2010m3
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jones_rw2010m3 mjones_rw2010m3
    abs_jones_rw2010m3 abs_mjones_rw2010m3
    , new_names           = cjones_rw2010m3 cmjones_rw2010m3
    cabs_jones_rw2010m3 cabs_mjones_rw2010m3
    );

/* Merge mjonesm1 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjonesm1
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm1 mjonesm1
    abs_jonesm1 abs_mjonesm1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjonesm1
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm1 mjonesm1
    abs_jonesm1 abs_mjonesm1
    , new_names           = cjonesm1 cmjonesm1
    cabs_jonesm1 cabs_mjonesm1
    );

/* Merge mjonesm2 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjonesm2
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm2 mjonesm2
    abs_jonesm2 abs_mjonesm2
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjonesm2
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm2 mjonesm2
    abs_jonesm2 abs_mjonesm2
    , new_names           = cjonesm2 cmjonesm2
    cabs_jonesm2 cabs_mjonesm2
    );

/* Merge mjonesm3 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjonesm3
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm3 mjonesm3
    abs_jonesm3 abs_mjonesm3
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjonesm3
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm3 mjonesm3
    abs_jonesm3 abs_mjonesm3
    , new_names           = cjonesm3 cmjonesm3
    cabs_jonesm3 cabs_mjonesm3
    );

/* Merge mjonesm4 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjonesm4
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm4 mjonesm4
    abs_jonesm4 abs_mjonesm4
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjonesm4
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm4 mjonesm4
    abs_jonesm4 abs_mjonesm4
    , new_names           = cjonesm4 cmjonesm4
    cabs_jonesm4 cabs_mjonesm4
    );

/* Merge mjonesm5 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjonesm5
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm5 mjonesm5
    abs_jonesm5 abs_mjonesm5
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjonesm5
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm5 mjonesm5
    abs_jonesm5 abs_mjonesm5
    , new_names           = cjonesm5 cmjonesm5
    cabs_jonesm5 cabs_mjonesm5
    );

/* Merge mjonesm6 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjonesm6
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm6 mjonesm6
    abs_jonesm6 abs_mjonesm6
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjonesm6
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm6 mjonesm6
    abs_jonesm6 abs_mjonesm6
    , new_names           = cjonesm6 cmjonesm6
    cabs_jonesm6 cabs_mjonesm6
    );

/* Merge mjonesm7 */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mjonesm7
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm7 mjonesm7
    abs_jonesm7 abs_mjonesm7
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mjonesm7
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = jonesm7 mjonesm7
    abs_jonesm7 abs_mjonesm7
    , new_names           = cjonesm7 cmjonesm7
    cabs_jonesm7 cabs_mjonesm7
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s11
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
