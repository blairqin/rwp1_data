/*
Output based on s13o2
cross sectional tests
using some common vars
*/

%let ver = s14o1;
%let dta = rwp1.s14;
%let of  = rwp1.&ver.d1;

data _tmp_rwp1_data;
    set &dta;
run;

%samp_sele_log(
    dsetin           = _tmp_rwp1_data
    , procedure      = %str(Compustat supplier-customer data, with positive sales, downloaded in Oct 2020)
    , samp_sele_out  = _tmp_rwp1_data_sele
    , drop_if_exists = Y
    );

/* Sample selection: fyear between 2003 and 2016 */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where 2003<=fyear<=2016;
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are not in the period between 2003 and 2016)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with auditors */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(audit_op_key);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations not in Audit Analytics.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier auditor us big4 firms */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where auditor_fkey in (1, 2, 3, 4)
        and auditor_country='USA';
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Supplier-years that are not audited by Big4 audit firms)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with not missing bankruptcy score */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(altmanz);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that have missing bankruptcy scores.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Financial industries */

%remove_sic(
    dsetin      = _tmp_rwp1_data
    , sic       = SIC
    , sic_digit = 4
    , start_sic = 6000
    , end_sic   = 6999
    , dsetout   = _tmp_rwp1_data
    );

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are in the financial industry (SIC from 6000 to 6999).)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Utilities Industry */

%remove_sic(
    dsetin      = _tmp_rwp1_data
    , sic       = SIC
    , sic_digit = 4
    , start_sic = 4400
    , end_sic   = 4999
    , dsetout   = _tmp_rwp1_data
    );

/* %samp_sele_log( */
/*     dsetin          = _tmp_rwp1_data */
/*     , procedure     = %str(Observations that are not in the regulated industry (SIC from 4400 to 4999)) */
/*     , samp_sele_out = _tmp_rwp1_data_sele */
/*     ); */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    gvkey_cgvkey = cats(gvkey, cgvkey);
    year = year(srcdate);
run;

/* Output */

%outputg2(
    dsetin         = _tmp_rwp1_data
    , wv           =
    abs_jones_atmcl1 abs_mjones_atmcl1
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1
    s_rdq_mostre_lm_log1

    , uv           =
    loss
    restructuring
    ma
    s_rdq_mostre_lm_d

    , um           =
    , um_new       =
    , cv           =
    gvkey cgvkey srcdate fyear sic sic1 sic2 sic3 sic4
    ffi5 ffi10 ffi12 ffi17 ffi30 ffi38 ffi48 ffi49 ffi_desp
    jones_rw2010 mjones_rw2010
    jones_atmcl1 mjones_atmcl1
    city_tenure
    gvkey_cgvkey
    auditor_fkey
    year
    , cv_new       =
    , cm           =
    , cm_new       =
    , keep_all_vars= Y
    , wb           =
    , wt           = winsor
    , wp           = 1 99
    , rstu_filter  = Y
    , rstu_dv      = abs_jones_atmcl1
    , rstu_ivs     = s_rdq_mostre_lm_d
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1
    loss
    restructuring
    ma

    , rstu_fe      = gvkey_cgvkey year
    , rstu_lc      = -2.5
    , rstu_lc_eq   = Y
    , rstu_rc      = 1.8
    , rstu_rc_eq   = Y
    , rstu_not_missing =
    , rv           = s_rdq_mostre_lm_log1
    rsi1 rsi2
    rsi_sic1 rsi_sic2 rsi_sic3 rsi_sic4
    , rv_new       = s_rdq_mostre_lm_log1_r
    rsi1_r rsi2_r
    rsi_sic1_r rsi_sic2_r rsi_sic3_r rsi_sic4_r
    , rb           =
    , rg           = 5
    , rnst         = Y
    , m_tab        = Y
    , m_stats      = mean stddev p25 p50 p75
    , m_fmts       = comma32.3 comma32.3 comma32.3 comma32.3 comma32.3
    , c_tab        = Y
    , f_tab        = Y
    , f_by         = ffi_desp
    , s_tab        = _tmp_rwp1_data_sele
    , of           = &of
    , pk           = gvkey cgvkey srcdate
    );

%let of  = rwp1.&ver.d2;

data _tmp_rwp1_data;
    set &dta;
run;

%samp_sele_log(
    dsetin           = _tmp_rwp1_data
    , procedure      = %str(Compustat supplier-customer data, with positive sales, downloaded in Oct 2020)
    , samp_sele_out  = _tmp_rwp1_data_sele
    , drop_if_exists = Y
    );

/* Sample selection: fyear between 2003 and 2016 */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where 2003<=fyear<=2016;
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are not in the period between 2003 and 2016)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with auditors */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(audit_op_key);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations not in Audit Analytics.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier auditor us big4 firms */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where auditor_fkey in (1, 2, 3, 4)
        and auditor_country='USA';
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Supplier-years that are not audited by Big4 audit firms)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with not missing bankruptcy score */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(altmanz);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that have missing bankruptcy scores.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Financial industries */

%remove_sic(
    dsetin      = _tmp_rwp1_data
    , sic       = SIC
    , sic_digit = 4
    , start_sic = 6000
    , end_sic   = 6999
    , dsetout   = _tmp_rwp1_data
    );

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are in the financial industry (SIC from 6000 to 6999).)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Utilities Industry */

%remove_sic(
    dsetin      = _tmp_rwp1_data
    , sic       = SIC
    , sic_digit = 4
    , start_sic = 4400
    , end_sic   = 4999
    , dsetout   = _tmp_rwp1_data
    );

/* %samp_sele_log( */
/*     dsetin          = _tmp_rwp1_data */
/*     , procedure     = %str(Observations that are not in the regulated industry (SIC from 4400 to 4999)) */
/*     , samp_sele_out = _tmp_rwp1_data_sele */
/*     ); */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    gvkey_cgvkey = cats(gvkey, cgvkey);
    year = year(srcdate);
run;

/* Output */

%outputg2(
    dsetin         = _tmp_rwp1_data
    , wv           =
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1
    s_rdq_mostre_lm_log1

    , uv           =
    restats_dummy
    loss
    restructuring
    ma
    s_rdq_mostre_lm_d

    , um           =
    , um_new       =
    , cv           =
    gvkey cgvkey srcdate fyear sic sic1 sic2 sic3 sic4
    ffi5 ffi10 ffi12 ffi17 ffi30 ffi38 ffi48 ffi49 ffi_desp
    city_tenure
    gvkey_cgvkey
    auditor_fkey
    year
    , cv_new       =
    , cm           =
    , cm_new       =
    , keep_all_vars= Y
    , wb           =
    , wt           = winsor
    , wp           = 1 99
    , rstu_filter  = Y
    , rstu_dv      = restats_dummy
    , rstu_ivs     = s_rdq_mostre_lm_log1
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1
    loss
    restructuring
    ma
    year
    , rstu_fe      = gvkey_cgvkey
    , rstu_lc      = -2.2
    , rstu_lc_eq   = Y
    , rstu_rc      = 2
    , rstu_rc_eq   = Y
    , rstu_not_missing =
    , rv           = s_rdq_mostre_lm_log1
    rsi1 rsi2
    rsi_sic1 rsi_sic2 rsi_sic3 rsi_sic4
    , rv_new       = s_rdq_mostre_lm_log1_r
    rsi1_r rsi2_r
    rsi_sic1_r rsi_sic2_r rsi_sic3_r rsi_sic4_r
    , rb           =
    , rg           = 7
    , rnst         = Y
    , m_tab        = Y
    , m_stats      = mean stddev p25 p50 p75
    , m_fmts       = comma32.3 comma32.3 comma32.3 comma32.3 comma32.3
    , c_tab        = Y
    , f_tab        = Y
    , f_by         = ffi_desp
    , s_tab        = _tmp_rwp1_data_sele
    , of           = &of
    , pk           = gvkey cgvkey srcdate
    );

%let of = rwp1.&ver.d3;

data _tmp_rwp1_data;
    set &dta;
run;

%samp_sele_log(
    dsetin           = _tmp_rwp1_data
    , procedure      = %str(Compustat supplier-customer data, with positive sales, downloaded in Oct 2020)
    , samp_sele_out  = _tmp_rwp1_data_sele
    , drop_if_exists = Y
    );

/* Sample selection: fyear between 2003 and 2016 */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where 2003<=fyear<=2016;
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are not in the period between 2003 and 2016)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with auditors */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(audit_op_key);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations not in Audit Analytics.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier auditor us big4 firms */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where auditor_fkey in (1, 2, 3, 4)
        and auditor_country='USA';
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Supplier-years that are not audited by Big4 audit firms)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with not missing bankruptcy score */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(altmanz);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that have missing bankruptcy scores.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Financial industries */

%remove_sic(
    dsetin      = _tmp_rwp1_data
    , sic       = SIC
    , sic_digit = 4
    , start_sic = 6000
    , end_sic   = 6999
    , dsetout   = _tmp_rwp1_data
    );

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are in the financial industry (SIC from 6000 to 6999).)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Utilities Industry */

/* %remove_sic( */
/*     dsetin      = _tmp_rwp1_data */
/*     , sic       = SIC */
/*     , sic_digit = 4 */
/*     , start_sic = 4400 */
/*     , end_sic   = 4999 */
/*     , dsetout   = _tmp_rwp1_data */
/*     ); */

/* %samp_sele_log( */
/*     dsetin          = _tmp_rwp1_data */
/*     , procedure     = %str(Observations that are not in the regulated industry (SIC from 4400 to 4999)) */
/*     , samp_sele_out = _tmp_rwp1_data_sele */
/*     ); */

/* Sample selection: Keep financially distressed firms */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where fdiss=1;
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Nonfinancially distressed firm-year observations.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    gvkey_cgvkey = cats(gvkey, cgvkey);
run;

/* Output */

%outputg2(
    dsetin    = _tmp_rwp1_data
    , wv      =
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1

    , uv      =
    loss
    restructuring
    ma
    going_concern
    s_rdq_mostre_lm_d
    s_rdq_mostre_lm_log1

    , um      =
    , um_new  =
    , cv      =
    gvkey cgvkey srcdate fyear sic sic1 sic2 sic3 sic4
    ffi5 ffi10 ffi12 ffi17 ffi30 ffi38 ffi48 ffi49 ffi_desp
    jones_rw2010 mjones_rw2010
    jones_atmcl1 mjones_atmcl1
    city_tenure
    gvkey_cgvkey
    auditor_fkey
    , cv_new  =
    , cm      =
    , cm_new  =
    , keep_all_vars= Y
    , wb      =
    , wt      = winsor
    , wp      = 1 99
    , rstu_filter  =
    , rstu_dv      = going_concern
    , rstu_ivs     = s_rdq_mostre_lm_d
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1
    loss
    restructuring
    ma
    , rstu_fe      = gvkey_cgvkey fyear auditor_fkey
    , rstu_lc      = -3
    , rstu_lc_eq   = Y
    , rstu_rc      = 3
    , rstu_rc_eq   = Y
    , rstu_not_missing = Y
    , rv           = s_rdq_mostre_lm_log1
    rsi1 rsi2
    rsi_sic1 rsi_sic2 rsi_sic3 rsi_sic4
    , rv_new       = s_rdq_mostre_lm_log1_r
    rsi1_r rsi2_r
    rsi_sic1_r rsi_sic2_r rsi_sic3_r rsi_sic4_r
    , rb      =
    , rg      = 7
    , rnst    = Y
    , m_tab   = Y
    , m_stats = mean stddev p25 p50 p75
    , m_fmts  = comma32.3 comma32.3 comma32.3 comma32.3 comma32.3
    , c_tab   = Y
    , f_tab   = Y
    , f_by    = ffi_desp
    , s_tab   = _tmp_rwp1_data_sele
    , of      = &of
    , pk      = gvkey cgvkey srcdate
    );

%let of = rwp1.&ver.d4;

data _tmp_rwp1_data;
    set &dta;
run;

%samp_sele_log(
    dsetin           = _tmp_rwp1_data
    , procedure      = %str(Compustat supplier-customer data, with positive sales, downloaded in Oct 2020)
    , samp_sele_out  = _tmp_rwp1_data_sele
    , drop_if_exists = Y
    );

/* Sample selection: fyear between 2003 and 2016 */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where 2003<=fyear<=2016;
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are not in the period between 2003 and 2016)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with auditors */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(audit_op_key);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations not in Audit Analytics.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier auditor us big4 firms */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where auditor_fkey in (1, 2, 3, 4)
        and auditor_country='USA';
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Supplier-years that are not audited by Big4 audit firms)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample selection: supplier firm with not missing bankruptcy score */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(altmanz);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that have missing bankruptcy scores.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Financial industries */

%remove_sic(
    dsetin      = _tmp_rwp1_data
    , sic       = SIC
    , sic_digit = 4
    , start_sic = 6000
    , end_sic   = 6999
    , dsetout   = _tmp_rwp1_data
    );

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations that are in the financial industry (SIC from 6000 to 6999).)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

/* Sample Selection: Drop Utilities Industry */

/* %remove_sic( */
/*     dsetin      = _tmp_rwp1_data */
/*     , sic       = SIC */
/*     , sic_digit = 4 */
/*     , start_sic = 4400 */
/*     , end_sic   = 4999 */
/*     , dsetout   = _tmp_rwp1_data */
/*     ); */

/* %samp_sele_log( */
/*     dsetin          = _tmp_rwp1_data */
/*     , procedure     = %str(Observations that are not in the regulated industry (SIC from 4400 to 4999)) */
/*     , samp_sele_out = _tmp_rwp1_data_sele */
/*     ); */

/* Sample selection: Keep supplier firms with analyst forecasts data from I/B/E/S */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    where not missing(ibtic);
run;

%samp_sele_log(
    dsetin          = _tmp_rwp1_data
    , procedure     = %str(Delete: Number of observations not in the I/B/E/S.)
    , samp_sele_out = _tmp_rwp1_data_sele
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    gvkey_cgvkey = cats(gvkey, cgvkey);
    year = year(srcdate);
run;

/* Output */

%outputg2(
    dsetin    = _tmp_rwp1_data
    , wv      =
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1
    numest_log1
    fcst_disp
    s_rdq_mostre_lm_log1

    , uv      =
    loss
    restructuring
    ma
    meet_or_beat_median
    s_rdq_mostre_lm_d

    , um      =
    , um_new  =
    , cv      =
    gvkey cgvkey srcdate fyear sic sic1 sic2 sic3 sic4
    ffi5 ffi10 ffi12 ffi17 ffi30 ffi38 ffi48 ffi49 ffi_desp
    city_tenure
    gvkey_cgvkey
    auditor_fkey
    year
    , cv_new  =
    , cm      =
    , cm_new  =
    , keep_all_vars= Y
    , wb      =
    , wt      = winsor
    , wp      = 1 99
    , rstu_filter  = L
    , rstu_dv      = meet_or_beat_median
    , rstu_ivs     = s_rdq_mostre_lm_d
    national_tenure
    size_log
    lev_demcl1
    roa_demcl1
    roa_demcl1_l1
    cfo_demcl1
    bm
    altmanz
    saleg_ndcl1_del1
    log_cfovm1_l3c
    rect_demcl1_l1
    payt_demcl1_l1
    numest_log1
    fcst_disp
    loss
    restructuring
    ma
    year

    , rstu_fe      = gvkey_cgvkey
    , rstu_lc      = -1.4
    , rstu_lc_eq   = Y
    , rstu_rc      = 1.4
    , rstu_rc_eq   = Y
    , rstu_not_missing =
    , rv           = s_rdq_mostre_lm_log1
    rsi1 rsi2
    rsi_sic1 rsi_sic2 rsi_sic3 rsi_sic4
    , rv_new       = s_rdq_mostre_lm_log1_r
    rsi1_r rsi2_r
    rsi_sic1_r rsi_sic2_r rsi_sic3_r rsi_sic4_r
    , rb      =
    , rg      = 7
    , rnst    = Y
    , m_tab   = Y
    , m_stats = mean stddev p25 p50 p75
    , m_fmts  = comma32.3 comma32.3 comma32.3 comma32.3 comma32.3
    , c_tab   = Y
    , f_tab   = Y
    , f_by    = ffi_desp
    , s_tab   = _tmp_rwp1_data_sele
    , of      = &of
    , pk      = gvkey cgvkey srcdate
    ); 