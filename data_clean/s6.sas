/*
Based on s5, using accruals from
Johnstone, K. M., C. Li, and S. Luo. 2014.
Client-Auditor Supply Chain Relationships, Audit Quality, and Audit Pricing.
AUDITING: A Journal of Practice & Theory 33 (4): 119–166.
*/

data _tmp_rwp1_data;
    set rwp1.s5;
run;

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_mjones_jll2014
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_jll2014 mjones_jll2014 abs_jones_jll2014 abs_mjones_jll2014
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_mjones_jll2014
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_jll2014 mjones_jll2014 abs_jones_jll2014 abs_mjones_jll2014
    , new_names           = cjones_jll2014 cmjones_jll2014 cabs_jones_jll2014 cabs_mjones_jll2014
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s6
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
