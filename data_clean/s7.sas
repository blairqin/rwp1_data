/*
Based on s6, adding accruals from
Reichelt, K. J., and D. Wang. 2010.
National and Office-Specific Measures of Auditor Industry Expertise and Effects on Audit Quality.
Journal of Accounting Research 48 (3): 647–686.
With and without intercept
Also merge with intercept from
Johnstone, K. M., C. Li, and S. Luo. 2014.
Client-Auditor Supply Chain Relationships, Audit Quality, and Audit Pricing.
AUDITING: A Journal of Practice & Theory 33 (4): 119–166.
*/

data _tmp_rwp1_data;
    set rwp1.s6;
run;

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_mjones_rw2010
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_rw2010 mjones_rw2010 abs_jones_rw2010 abs_mjones_rw2010
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_mjones_rw2010
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_rw2010 mjones_rw2010 abs_jones_rw2010 abs_mjones_rw2010
    , new_names           = cjones_rw2010 cmjones_rw2010 cabs_jones_rw2010 cabs_mjones_rw2010
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_mjones_jll2014m1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_jll2014m1 mjones_jll2014m1 abs_jones_jll2014m1 abs_mjones_jll2014m1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_mjones_jll2014m1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_jll2014m1 mjones_jll2014m1 abs_jones_jll2014m1 abs_mjones_jll2014m1
    , new_names           = cjones_jll2014m1 cmjones_jll2014m1 cabs_jones_jll2014m1 cabs_mjones_jll2014m1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_mjones_rw2010m1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_rw2010m1 mjones_rw2010m1 abs_jones_rw2010m1 abs_mjones_rw2010m1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_mjones_rw2010m1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jones_rw2010m1 mjones_rw2010m1 abs_jones_rw2010m1 abs_mjones_rw2010m1
    , new_names           = cjones_rw2010m1 cmjones_rw2010m1 cabs_jones_rw2010m1 cabs_mjones_rw2010m1
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s7
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
