/*
Based on s4
Add new auditor search that removes index page, unsuccessful search
, and robot search
*/

data _tmp_rwp1_data;
    set rwp1.stage4;
run;

/* Get new auditor search data */

data _tmp_auditor_search_10kq;
    set elog.auditor_search_nocrawler_ft(
        where=(
        (find(form_type, '10-K', 'i')>0
        or find(form_type, '10K', 'i')>0
        or find(form_type, '10-Q', 'i')>0
        or find(form_type, '10Q', 'i')>0)
        and (auditor_id not in (
        13, 18, 20, 27, 29, 34, 40, 41
        ))
        ));
run;

%means(
    dsetin             = _tmp_auditor_search_10kq
    , by               = auditor_fkey date cik
    , allow_missing_by = N
    , variables        = htm xml txt xbrl other
    , method           = sum
    , new_names        = htm xml txt xbrl other
    , mean_out         = _tmp_auditor_search_1
);

%include "./rwp1_data/match_search.sas";

/* Auditor info search from -365 to filing_date */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_fdate_htm s_fdate_xml s_fdate_txt s_fdate_xbrl s_fdate_other
    );

/* Auditor info search from -365 to rdq */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_rdq_htm s_rdq_xml s_rdq_txt s_rdq_xbrl s_rdq_other
    );

/* Auditor search without robots using lm method */

data _tmp_auditor_search_1;
    set _tmp_auditor_search_10kq;
    where lm=0;
run;

%means(
    dsetin             = _tmp_auditor_search_1
    , by               = auditor_fkey date cik
    , allow_missing_by = N
    , variables        = htm xml txt xbrl other
    , method           = sum
    , new_names        = htm xml txt xbrl other
    , mean_out         = _tmp_auditor_search_1
);

/* Auditor info search from -365 to filing_date */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_fdate_lm_htm s_fdate_lm_xml s_fdate_lm_txt s_fdate_lm_xbrl s_fdate_lm_other
    );

/* Auditor info search from -365 to rdq */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_rdq_lm_htm s_rdq_lm_xml s_rdq_lm_txt s_rdq_lm_xbrl s_rdq_lm_other
    );

/* Auditor search without robots using drt method */

data _tmp_auditor_search_1;
    set _tmp_auditor_search_10kq;
    where drt=0;
run;

%means(
    dsetin             = _tmp_auditor_search_1
    , by               = auditor_fkey date cik
    , allow_missing_by = N
    , variables        = htm xml txt xbrl other
    , method           = sum
    , new_names        = htm xml txt xbrl other
    , mean_out         = _tmp_auditor_search_1
);

/* Auditor info search from -365 to filing_date */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_fdate_drt_htm s_fdate_drt_xml s_fdate_drt_txt s_fdate_drt_xbrl s_fdate_drt_other
    );

/* Auditor info search from -365 to rdq */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_rdq_drt_htm s_rdq_drt_xml s_rdq_drt_txt s_rdq_drt_xbrl s_rdq_drt_other
    );

/* Auditor search without robots using rpv method */

data _tmp_auditor_search_1;
    set _tmp_auditor_search_10kq;
    where rpv=0;
run;

%means(
    dsetin             = _tmp_auditor_search_1
    , by               = auditor_fkey date cik
    , allow_missing_by = N
    , variables        = htm xml txt xbrl other
    , method           = sum
    , new_names        = htm xml txt xbrl other
    , mean_out         = _tmp_auditor_search_1
);

/* Auditor info search from -365 to filing_date */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_fdate_rpv_htm s_fdate_rpv_xml s_fdate_rpv_txt s_fdate_rpv_xbrl s_fdate_rpv_other
    );

/* Auditor info search from -365 to rdq */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_1
    , search_vars       = htm xml txt xbrl other
    , new_name          = s_rdq_rpv_htm s_rdq_rpv_xml s_rdq_rpv_txt s_rdq_rpv_xbrl s_rdq_rpv_other
    );

%macro gen_log(
    dsetin =
    , vars =
    , name =
    );

    %local i var_i;

    data &dsetin;
        set &dsetin;
        if cmiss(of &vars)=0 then do;
            &name        = sum(of &vars);
            &name._dummy = 1;
            log_&name    = log(1 + sum(of &vars));
            %do i=1 %to %sysfunc(countw(&vars, %str( )));
                %let var_i    = %scan(&vars, &i, %str( ));
                log_&var_i    = log(1 + &var_i);
                &var_i._dummy = (&var_i. > 0);
                %end;
            end;
        else if 2003<=fyear<=2016 then do;
            &name        = 0;
            &name._dummy = 0;
            log_&name    = 0;
            %do i=1 %to %sysfunc(countw(&vars, %str( )));
                %let var_i    = %scan(&vars, &i, %str( ));
                log_&var_i    = 0;
                &var_i._dummy = 0;
                &var_i        = 0;
                %end;
            end;
        else call missing(of
            log_&name &name._dummy
            %do i=1 %to %sysfunc(countw(&vars, %str( )));
                %let var_i    = %scan(&vars, &i, %str( ));
                &var_i.
                %end;
            );
    run;
%mend gen_log;

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_fdate_htm s_fdate_xml s_fdate_txt s_fdate_xbrl s_fdate_other
    , name = s_fdate
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_rdq_htm s_rdq_xml s_rdq_txt s_rdq_xbrl s_rdq_other
    , name = s_rdq
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_fdate_lm_htm s_fdate_lm_xml s_fdate_lm_txt s_fdate_lm_xbrl s_fdate_lm_other
    , name = s_fdate_lm
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_rdq_lm_htm s_rdq_lm_xml s_rdq_lm_txt s_rdq_lm_xbrl s_rdq_lm_other
    , name = s_rdq_lm
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_fdate_drt_htm s_fdate_drt_xml s_fdate_drt_txt s_fdate_drt_xbrl s_fdate_drt_other
    , name = s_fdate_drt
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_rdq_drt_htm s_rdq_drt_xml s_rdq_drt_txt s_rdq_drt_xbrl s_rdq_drt_other
    , name = s_rdq_drt
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_fdate_rpv_htm s_fdate_rpv_xml s_fdate_rpv_txt s_fdate_rpv_xbrl s_fdate_rpv_other
    , name = s_fdate_rpv
    );

%gen_log(
    dsetin = _tmp_rwp1_data
    , vars = s_rdq_rpv_htm s_rdq_rpv_xml s_rdq_rpv_txt s_rdq_rpv_xbrl s_rdq_rpv_other
    , name = s_rdq_rpv
    );

/* Merge meet or beat analyst forecasts */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = ibtic srcdate
    , right_table         = ibes.mb_qtr_epsus
    , right_table_index   = ticker fpedats
    , allow_missing_index = N
    , variables           = meet_mean beat_mean meet_or_beat_mean
    meet_median beat_median meet_or_beat_median
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cibtic cdatadate
    , right_table         = ibes.mb_qtr_epsus
    , right_table_index   = ticker fpedats
    , allow_missing_index = N
    , variables           = meet_mean beat_mean meet_or_beat_mean
    meet_median beat_median meet_or_beat_median
    , new_names           = cmeet_mean cbeat_mean cmeet_or_beat_mean
    cmeet_median cbeat_median cmeet_or_beat_median
    );

/* Merge number of analyst following */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = ibtic srcdate
    , right_table         = ibes.numest_qtr_epsus
    , right_table_index   = ticker fpedats
    , allow_missing_index = N
    , variables           = numest numest_log1
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cibtic cdatadate
    , right_table         = ibes.numest_qtr_epsus
    , right_table_index   = ticker fpedats
    , allow_missing_index = N
    , variables           = numest numest_log1
    , new_names           = cnumest cnumest_log1
    );

/* Merge forcast dispersion */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = ibtic srcdate
    , right_table         = ibes.fcst_disp_qtr_epsus
    , right_table_index   = ticker fpedats
    , allow_missing_index = N
    , variables           = fcst_disp
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cibtic cdatadate
    , right_table         = ibes.fcst_disp_qtr_epsus
    , right_table_index   = ticker fpedats
    , allow_missing_index = N
    , variables           = fcst_disp
    , new_names           = cfcst_disp
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s5
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
