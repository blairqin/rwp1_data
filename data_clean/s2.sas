/* Stage 2 merge variables */
/* Merge all variables from Gipper, Hail, and Leuz 2020 TAR */
/* Table I-1 */

data _tmp_rwp1_data;
    set rwp1.stage1;
run;

/* Merge supplier's auditor information */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = audit_op_key
    , right_table         = audit.auditopin
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = auditor_fkey sig_date_of_op_s
        going_concern auditor_country auditor_state auditor_city
        ftp_file_fkey file_date http_name_text
    , new_names           =
    );

/* Merge customer's auditor information */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = caudit_op_key
    , right_table         = audit.auditopin
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = auditor_fkey sig_date_of_op_s
        going_concern auditor_country auditor_state auditor_city
        ftp_file_fkey file_date http_name_text
    , new_names           = cauditor_fkey csig_date_of_op_s
        cgoing_concern cauditor_country cauditor_state cauditor_city
        cftp_file_fkey cfile_date chttp_name_text
    );

/* Merge supplier's report date from compustat */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate fyr
    , right_table         = comp.fundq_filtered
    , right_table_index   = gvkey datadate fyr
    , allow_missing_index = N
    , variables           = rdq
    );

/* Merge customer's report date from compustat */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate cfyr
    , right_table         = comp.fundq_filtered
    , right_table_index   = gvkey datadate fyr
    , allow_missing_index = N
    , variables           = rdq
    , new_names           = crdq
    );

* Merge variables from Gipper, Hail, and Leuz 2020 TAR;

* Supplier discretionary accruals;

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_mjones
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jitacfatl1roabysic2fyr mjitacfatl1roabysic2fyr
        jitacfatmcl1roabysic2fyr mjitacfatmcl1roabysic2fyr
    , new_names           = jones_atl1 mjones_atl1 jones_atmcl1 mjones_atmcl1
    );

* Customer discretionary accruals;

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_mjones
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = jitacfatl1roabysic2fyr mjitacfatl1roabysic2fyr
        jitacfatmcl1roabysic2fyr mjitacfatmcl1roabysic2fyr
    , new_names           = cjones_atl1 cmjones_atl1 cjones_atmcl1 cmjones_atmcl1
    );

/* Absolute valuve of discretionary accruals */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    abs_jones_atl1     = abs(jones_atl1);
    abs_mjones_atl1    = abs(mjones_atl1);
    abs_jones_atmcl1   = abs(jones_atmcl1);
    abs_mjones_atmcl1  = abs(mjones_atmcl1);
    abs_cjones_atl1    = abs(cjones_atl1);
    abs_cmjones_atl1   = abs(cmjones_atl1);
    abs_cjones_atmcl1  = abs(cjones_atmcl1);
    abs_cmjones_atmcl1 = abs(cmjones_atmcl1);
run;

/* Suppliers' financial restatements */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = audit_op_key
    , right_table         = audit.audit_restates
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = no_of_res_1 res_dummy_1 no_of_res_2 res_dummy_2
    , new_names           = announce_res announce_res_dummy restats restats_dummy
    );

/* Customers' financial restatements */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = caudit_op_key
    , right_table         = audit.audit_restates
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = no_of_res_1 res_dummy_1 no_of_res_2 res_dummy_2
    , new_names           = cannounce_res cannounce_res_dummy crestats crestats_dummy
    );

/* Suppliers' 404 internal control weaknesses */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = audit_op_key
    , right_table         = audit.audit_icws
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = icws_7
    , new_names           = icws
    );

/* Customers' 404 internal control weaknesses */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = caudit_op_key
    , right_table         = audit.audit_icws
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = icws_7
    , new_names           = cicws
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if not missing(audit_op_key) and missing(icws)
        then icws = 0;
    if not missing(caudit_op_key) and missing(cicws)
        then cicws = 0;
run;

/* Merge auditor tenure */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = audit_op_key
    , right_table         = audit.auditor_tenure
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = tenure_1 tenure_2
    , new_names           = national_tenure city_tenure
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = caudit_op_key
    , right_table         = audit.auditor_tenure
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = tenure_1 tenure_2
    , new_names           = cnational_tenure ccity_tenure
    );

/* Merge market value */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_mv
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = mv_log mv_l1_log mv_mcl1_log
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_mv
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = mv_log mv_l1_log mv_mcl1_log
    , new_names           = cmv_log cmv_l1_log cmv_mcl1_log
    );

/* Merge leverage */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_debt
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = debt debt_del1 debt_demcl1
    , new_names           = lev lev_del1 lev_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_debt
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = debt debt_del1 debt_demcl1
    , new_names           = clev clev_del1 clev_demcl1
    );

/* Merge ROA and lagged ROA */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_roam1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = roam1 roam1_del1 roam1_demcl1
    , new_names           = roa roa_del1 roa_demcl1
    );

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_roam1
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = roam1 roam1_del1 roam1_demcl1
    , new_names           = roa_l1 roa_del1_l1 roa_demcl1_l1
    , allow_hash          = N
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_roam1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = roam1 roam1_del1 roam1_demcl1
    , new_names           = croa croa_del1 croa_demcl1
    );

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_roam1
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = roam1 roam1_del1 roam1_demcl1
    , new_names           = croa_l1 croa_del1_l1 croa_demcl1_l1
    , allow_hash          = N
    );

/* Merge loss */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_loss
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = loss loss_l1
    , new_names           = loss loss_l1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_loss
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = loss loss_l1
    , new_names           = closs closs_l1
    );

/* Merge CFO */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_cfo
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = cfo cfo_del1 cfo_demcl1
    , new_names           = cfo cfo_del1 cfo_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_cfo
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = cfo cfo_del1 cfo_demcl1
    , new_names           = ccfo ccfo_del1 ccfo_demcl1
    );

/* Merge book to market */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_bm
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = bm bm_del1 bm_demcl1
    , new_names           = bm bm_del1 bm_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_bm
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = bm bm_del1 bm_demcl1
    , new_names           = cbm cbm_del1 cbm_demcl1
    );

/* Merge Altman Z-Score */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_bankruptcy
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = bankruptcy_m3
    , new_names           = altmanz
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_bankruptcy
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = bankruptcy_m3
    , new_names           = caltmanz
    );

/* Merge sale growth */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_saleg
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = saleg_ndcl1 saleg_ndcl1_del1 saleg_ndcl1_demcl1
    , new_names           = saleg_ndcl1 saleg_ndcl1_del1 saleg_ndcl1_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_saleg
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = saleg_ndcl1 saleg_ndcl1_del1 saleg_ndcl1_demcl1
    , new_names           = csaleg_ndcl1 csaleg_ndcl1_del1 csaleg_ndcl1_demcl1
    );

/* Merge log of cfo volatility */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_cfovm1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = cfovm1_l3c cfovm1_l4c cfovm1_l5c
    , new_names           = cfovm1_l3c cfovm1_l4c cfovm1_l5c
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_cfovm1
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = cfovm1_l3c cfovm1_l4c cfovm1_l5c
    , new_names           = ccfovm1_l3c ccfovm1_l4c ccfovm1_l5c
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    log_cfovm1_l3c  = log(cfovm1_l3c);
    log_cfovm1_l4c  = log(cfovm1_l4c);
    log_cfovm1_l5c  = log(cfovm1_l5c);
    log_ccfovm1_l3c = log(ccfovm1_l3c);
    log_ccfovm1_l4c = log(ccfovm1_l4c);
    log_ccfovm1_l5c = log(ccfovm1_l5c);
run;

/* Merge restructuring firm */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_restruct
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = restructm2
    , new_names           = restructuring
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_restruct
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = restructm2
    , new_names           = crestructuring
    );

/* Merge m&a firm */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_ma
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = mam2
    , new_names           = ma
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_ma
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = mam2
    , new_names           = cma
    );

/* Merge receivables turnover */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_rect
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = rect rect_del1 rect_demcl1
    , new_names           = rect rect_del1 rect_demcl1
    );

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_rect
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = rect rect_del1 rect_demcl1
    , new_names           = rect_l1 rect_del1_l1 rect_demcl1_l1
    , allow_hash          = N
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_rect
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = rect rect_del1 rect_demcl1
    , new_names           = crect crect_del1 crect_demcl1
    );

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_rect
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = rect rect_del1 rect_demcl1
    , new_names           = crect_l1 crect_del1_l1 crect_demcl1_l1
    , allow_hash          = N
    );

/* Merge payables turnover */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_payt
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = payt payt_del1 payt_demcl1
    , new_names           = payt payt_del1 payt_demcl1
    );

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_payt
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = payt payt_del1 payt_demcl1
    , new_names           = payt_l1 payt_del1_l1 payt_demcl1_l1
    , allow_hash          = N
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_payt
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = payt payt_del1 payt_demcl1
    , new_names           = cpayt cpayt_del1 cpayt_demcl1
    );

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_payt
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = payt payt_del1 payt_demcl1
    , new_names           = cpayt_l1 cpayt_del1_l1 cpayt_demcl1_l1
    , allow_hash          = N
    );

/* Merge audit fees */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = audit_op_key
    , right_table         = audit.auditopin
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = closestfy_sum_audfees closestfy_sum_total
    , new_names           = audfees total_audfees
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = caudit_op_key
    , right_table         = audit.auditopin
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = closestfy_sum_audfees closestfy_sum_total
    , new_names           = caudfees ctotal_audfees
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    log_audfees        = log(audfees);
    log_total_audfees  = log(total_audfees);
    log_caudfees       = log(caudfees);
    log_ctotal_audfees = log(ctotal_audfees);
run;

/* Merge current assets */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_ca
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = ca ca_del1 ca_demcl1
    , new_names           = ca ca_del1 ca_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_ca
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = ca ca_del1 ca_demcl1
    , new_names           = cca cca_del1 cca_demcl1
    );

/* Report lag */

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if cmiss(of srcdate file_date)=0
        then rep_lag = file_date - srcdate;
    else call missing(rep_lag);
    if cmiss(of cdatadate cfile_date)=0
        then crep_lag = cfile_date - cdatadate;
    else call missing(crep_lag);
run;

/* Merge business segments */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_segs
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = busseg_4 busseg_6
    , new_names           = busseg busseg_log
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_segs
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = busseg_4 busseg_6
    , new_names           = cbusseg cbusseg_log
    );

/* Merge foreign sales */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_fsale
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = fsale fsale_del1 fsale_demcl1
    , new_names           = fsale fsale_del1 fsale_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_fsale
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = fsale fsale_del1 fsale_demcl1
    , new_names           = cfsale cfsale_del1 cfsale_demcl1
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if not missing(gvkey) and missing(fsale)
        then fsale = 0;
    if not missing(gvkey) and missing(fsale_del1)
        then fsale_del1 = 0;
    if not missing(gvkey) and missing(fsale_demcl1)
        then fsale_demcl1 = 0;
    if not missing(cgvkey) and missing(cfsale)
        then cfsale = 0;
    if not missing(cgvkey) and missing(cfsale_del1)
        then cfsale_del1 = 0;
    if not missing(cgvkey) and missing(cfsale_demcl1)
        then cfsale_demcl1 = 0;
run;

/* Merge size */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_size
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = size size_l1 size_mcl1
    size_log size_l1_log size_mcl1_log
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_size
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = size size_l1 size_mcl1
    size_log size_l1_log size_mcl1_log
    , new_names           = csize csize_l1 csize_mcl1
    csize_log csize_l1_log csize_mcl1_log
    );


/* Output data */

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.stage2
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
