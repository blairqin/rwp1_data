/*
rwp1 stage3
Merge auditor information search variables
*/

data _tmp_rwp1_data;
    set rwp1.stage2;
run;

/* Get auditor edgar log data */

data _tmp_auditor_search_10kq;
    set elog.auditor_search_form_type(
        where=(
        find(form_type, '10-K', 'i')>0
        or find(form_type, '10K', 'i')>0
        or find(form_type, '10-Q', 'i')>0
        or find(form_type, '10Q', 'i')>0
        ));
run;

%means(
    dsetin             = _tmp_auditor_search_10kq
    , by               = auditor_id auditor_fkey date cik
    , allow_missing_by = N
    , variables        = count
    , method           = sum
    , new_names        = count
    , mean_out         = _tmp_auditor_search_10kq
);

/* Import auditor information search macro */

%include "./rwp1_data/match_search.sas";

/* Remove auditor search from outside US */
/* Auditor ids: 13 18 20 27 29 34 40 41 */

data _tmp_auditor_search_10kq_usa;
    set _tmp_auditor_search_10kq;
    where auditor_id not in (
        13, 18, 20, 27, 29, 34, 40, 41
        );
run;

/* Auditor info search from -365 to filing_date */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = file_date
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_10kq_usa
    , new_name          = asearch_filing_date
    );

/* Auditor info search from -365 to rdp */

%match_search(
    dsetin              = _tmp_rwp1_data
    , cik               = ccik
    , auditor           = auditor_fkey
    , date              = rdq
    , range_start       = -364
    , range_end         = 0
    , aud_search_by_day = _tmp_auditor_search_10kq_usa
    , new_name          = asearch_rdq
);


/* Generate log and dummy of auditor search */

%let var_name = asearch_filing_date;
data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if not missing(&var_name) then do;
        log_&var_name    = log(&var_name + 1);
        &var_name._dummy = 1;
        end;
    else if 2003<=fyear<=2016 then do;
        log_&var_name    = 0;
        &var_name.       = 0;
        &var_name._dummy = 0;
        end;
    else do;
        call missing(log_&var_name, &var_name._dummy);
        end;
run;

%let var_name = asearch_rdq;
data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if not missing(&var_name) then do;
        log_&var_name    = log(&var_name + 1);
        &var_name._dummy = 1;
        end;
    else if 2003<=fyear<=2016 then do;
        log_&var_name    = 0;
        &var_name.       = 0;
        &var_name._dummy = 0;
        end;
    else do;
        call missing(log_&var_name, &var_name._dummy);
        end;
run;

/* Output Stag 1 data as rwp1.stage1 */

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.stage3
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
