/*
Based on s7
Merge control variables from JLL2014
Johnstone, K. M., C. Li, and S. Luo. 2014.
Client-Auditor Supply Chain Relationships,
Audit Quality, and Audit Pricing.
AUDITING: A Journal of Practice & Theory 33 (4): 119–166.
*/

data _tmp_rwp1_data;
    set rwp1.s7;
    if not missing(national_tenure)
        then log1_national_tenure = log(1+national_tenure);
    else call missing(log1_national_tenure);
    if not missing(city_tenure)
        then log1_city_tenure = log(1+city_tenure);
    else call missing(log1_city_tenure);
    if not missing(cnational_tenure)
        then log1_cnational_tenure = log(1+cnational_tenure);
    else call missing(log1_cnational_tenure);
    if not missing(ccity_tenure)
        then log1_ccity_tenure = log(1+ccity_tenure);
    else call missing(log1_ccity_tenure);
run;

/* Merge roa ni / at */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_roam1
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = roam1 roam1_del1 roam1_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_roam1
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = roam1 roam1_del1 roam1_demcl1
    , new_names           = croam1 croam1_del1 croam1_demcl1
    );

/* Merge national and industry leader */
/* Also calculate joint leader */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = audit_op_key
    , right_table         = audit.auditor_leader
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = leader_2 leader_3
    , new_names           = leader_country leader_city
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = caudit_op_key
    , right_table         = audit.auditor_leader
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           = leader_2 leader_3
    , new_names           = cleader_country cleader_city
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if cmiss(of leader_country leader_city) = 0
        then joint_leader = (leader_country=leader_city=1);
    else call missing(joint_leader);
    if cmiss(of cleader_country cleader_city) = 0
        then cjoint_leader = (cleader_country=cleader_city=1);
    else call missing(cjoint_leader);
run;

/* Merge total accruals */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_ta
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = ta ta_del1 ta_demcl1
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_ta
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = ta ta_del1 ta_demcl1
    , new_names           = cta cta_del1 cta_demcl1
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if not missing(ta) then abs_ta = abs(ta);
    else call missing(abs_ta);
    if not missing(ta_del1) then abs_ta_del1 = abs(ta_del1);
    else call missing(abs_ta_del1);
    if not missing(ta_demcl1) then abs_ta_demcl1 = abs(ta_demcl1);
    else call missing(abs_ta_demcl1);
    if not missing(cta) then abs_cta = abs(cta);
    else call missing(abs_cta);
    if not missing(cta_del1) then abs_cta_del1 = abs(cta_del1);
    else call missing(abs_cta_del1);
    if not missing(cta_demcl1) then abs_cta_demcl1 = abs(cta_demcl1);
    else call missing(abs_cta_demcl1);
run;

/* Merge standard deviation of cfo */

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_cfov
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = cfo_del1_v_l3c
    , new_names           = cfo_del1_v_l3c_l1
    , allow_hash          = N
    );

%merge_leadlag_by_year(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_cfov
    , right_table_index   = gvkey
    , year                = fyear
    , allow_missing_index = N
    , gaps                = -1
    , variables           = cfo_del1_v_l3c
    , new_names           = ccfo_del1_v_l3c_l1
    , allow_hash          = N
    );

/* Merge research and development ratio */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_rd
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = rd rd_del1 rd_demcl1
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_rd
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = rd rd_del1 rd_demcl1
    , new_names           = crd crd_del1 crd_demcl1
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if missing(rd)         then rd=0;
    if missing(rd_del1)    then rd_del1=0;
    if missing(rd_demcl1)  then rd_demcl1=0;
    if missing(crd)        then crd=0;
    if missing(crd_del1)   then crd_del1=0;
    if missing(crd_demcl1) then crd_demcl1=0;
run;

/* Merge litigation */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_litigation
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = litigation
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_litigation
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = litigation
    , new_names           = clitigation
    );

/* Merge market value to book value */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_mb
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = mb mb_del1 mb_demcl1
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_mb
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = mb mb_del1 mb_demcl1
    , new_names           = cmb cmb_del1 cmb_demcl1
    );

/* Merge sale growth */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_salegm1
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = salegm1_ndcl1 salegm1_ndcl1_del1 salegm1_ndcl1_demcl1
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_salegm1
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = salegm1_ndcl1 salegm1_ndcl1_del1 salegm1_ndcl1_demcl1
    , new_names           = csalegm1_ndcl1 csalegm1_ndcl1_del1 csalegm1_ndcl1_demcl1
    );

/* Merge altmanz */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey srcdate
    , right_table         = comp.funda_bankruptcy
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = bankruptcy_m1 bankruptcy_m2
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cdatadate
    , right_table         = comp.funda_bankruptcy
    , right_table_index   = gvkey datadate
    , allow_missing_index = N
    , variables           = bankruptcy_m1 bankruptcy_m2
    , new_names           = cbankruptcy_m1 cbankruptcy_m2
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s8
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
