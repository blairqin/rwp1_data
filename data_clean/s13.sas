/*
Merge financially distress variables
*/

data _tmp_rwp1_data;
    set rwp1.s12;
run;

/* Merge fdiss vars */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = gvkey fyear
    , right_table         = comp.funda_fidss
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = fdiss
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cgvkey cfyear
    , right_table         = comp.funda_fidss
    , right_table_index   = gvkey fyear
    , allow_missing_index = N
    , variables           = fdiss
    , new_names           = cfdiss
    );

%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s13
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
