/*
Based on s11
Merge restatements variables
*/

data _tmp_rwp1_data;
    set rwp1.s10;
run;

/* Merge Francis, Michas, and Yu 2013 restatements */

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = audit_op_key
    , right_table         = audit.audit_restates_fmy2013
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           =
    nos_res_fmy2013_1 res_d_fmy2013_1
    nos_res_fmy2013_2 res_d_fmy2013_2
    nos_res_fmy2013_3 res_d_fmy2013_3
    nos_res_fmy2013_4 res_d_fmy2013_4
    nos_res_fmy2013_5 res_d_fmy2013_5
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = caudit_op_key
    , right_table         = audit.audit_restates_fmy2013
    , right_table_index   = audit_op_key
    , allow_missing_index = N
    , variables           =
    nos_res_fmy2013_1 res_d_fmy2013_1
    nos_res_fmy2013_2 res_d_fmy2013_2
    nos_res_fmy2013_3 res_d_fmy2013_3
    nos_res_fmy2013_4 res_d_fmy2013_4
    nos_res_fmy2013_5 res_d_fmy2013_5
    , new_names           =
    cnos_res_fmy2013_1 cres_d_fmy2013_1
    cnos_res_fmy2013_2 cres_d_fmy2013_2
    cnos_res_fmy2013_3 cres_d_fmy2013_3
    cnos_res_fmy2013_4 cres_d_fmy2013_4
    cnos_res_fmy2013_5 cres_d_fmy2013_5
    );

data _tmp_rwp1_data;
    set _tmp_rwp1_data;
    if cmiss(of
        auditor_fkey auditor_country
        auditor_state auditor_city
        )=0
        then common_auditor = (
        auditor_fkey = cauditor_fkey
        and auditor_country = cauditor_country
        and auditor_state = cauditor_state
        and auditor_city = cauditor_city
        );
    else call missing(common_auditor);
run;

/* Merge ffi48 description */

data _tmp_ffi48;
    set psec.siccodes48;
proc sort data=_tmp_ffi48 nodupkey;
    by ffi;
run;

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = ffi48
    , right_table         = _tmp_ffi48
    , right_table_index   = ffi
    , allow_missing_index = N
    , variables           = ffi_desp
    , new_names           =
    );

%merge_left_join(
    left_table            = _tmp_rwp1_data
    , left_table_index    = cffi48
    , right_table         = _tmp_ffi48
    , right_table_index   = ffi
    , allow_missing_index = N
    , variables           = ffi_desp
    , new_names           = cffi_desp
    );


%copy_data(
    dsetin     = _tmp_rwp1_data
    , dsetout  = rwp1.s12
    , pk       = gvkey cgvkey srcdate
    , not_null = salecs
    );
