from rwp1_data.output_functions import parse_tab, add_header_footer, add_row_color, reformat_header
from rwp1_data import loc
import pandas as pd
import os
import sys
import json

if __name__ == '__main__':
    pd.options.display.max_colwidth = 1000
    data_ver = 's13o2'
    # Get all tables needed to output
    tables = [
        'd1_sele', 'd2_sele', 'd4_sele',
        'd1_sum', 'd2_sum', 'd4_sum',
        'd1_freq', 'd2_freq', 'd4_freq'
    ]
    for num, table in enumerate(tables):
        tables[num] = os.path.join(loc.data, '{0}{1}.sas7bdat'.format(data_ver, table))
    samples = [
        '$\\left|\\textit{Jones DA}\\right|$ and $\\left|\\textit{MJones DA}\\right|$',
        '\\textit{Restate}',
        '\\textit{Meet or Beat}'
    ]
    # Titles, preambles, and column formats of selected tables
    table_titles = []
    table_preambles = []
    table_column_formats = []
    for sample in samples:
        table_titles.append('Sample Selection (Sample using {0} as Dependent Variable(s))'.format(sample))
        table_preambles.append('\\sisetup{table-space-text-pre={(}, table-space-text-post={)}}')
        table_column_formats.append('>{\\kern-\\tabcolsep}l S[table-format=6.0]<{\\kern-\\tabcolsep}')
    for sample in samples:
        table_titles.append('Summary Statistics (Sample using {0} as Dependent Variable(s))'.format(sample))
        table_preambles.append('\\sisetup{table-space-text-pre=, table-space-text-post=}')
        table_column_formats.append('>{\\kern-\\tabcolsep}l *{5}{S[table-format=-3.3]}<{\\kern-\\tabcolsep}')
    for sample in samples:
        table_titles.append('Fama French 48 Industry Distribution ' +
                            '(Sample using {0} as Dependent Variable(s))'.format(sample))
        table_preambles.append('\\sisetup{table-space-text-pre=, table-space-text-post=}')
        table_column_formats.append('>{\\kern-\\tabcolsep}l S[table-format=6.0]' +
                              'S[table-format=3.3, table-space-text-post={\%}]<{\\kern-\\tabcolsep}')
    table_files = []
    for num, table in enumerate(tables):
        table_title = table_titles[num]
        table_preamble = table_preambles[num]
        table_column_format = table_column_formats[num]
        df = pd.read_sas(table, encoding='UTF-8')
        t = parse_tab(df=df)
        s = add_header_footer(t=t, tbl_preamble=table_preamble, col_fmt=table_column_format, title=table_title)
        num_str = str(num).zfill(3)
        table_file_name = 'table_{0}.tex'.format(num_str)
        with open(table_file_name, 'w') as f:
            f.write(s)
        table_files.append(table_file_name)
    # Parse all regression tables
    for reg_table_file in sorted(os.listdir()):
        if reg_table_file.startswith('reg_table') & reg_table_file.endswith('.tex'):
            t = parse_tab(file=reg_table_file)
            with open(reg_table_file, 'w') as f:
                f.write(t[0])
            table_files.append(reg_table_file)
    s = r"""
    \documentclass[singlespacing, times]{wpaper}
    \addbibresource{../rwp1.bib}
    \sisetup{
        detect-all,
        table-alignment=center,
        table-text-alignment=center,
        input-signs= + -,
        input-ignore={,},
        input-decimal-markers={.},
        group-separator={,},
        group-minimum-digits=4,
        table-align-text-pre=false,
        input-open-uncertainty=,
        input-close-uncertainty=,
        table-align-text-post=false
    }
    \begin{document}
    """
    for table_file in table_files:
        s = s + r'''
        \input{{{0}}}
        '''.format(table_file)
    s = s + r'''
    \clearpage
    {\setstretch{1}
    \printbibliography}
    \end{document}'''
    file_name = os.path.basename(sys.argv[0]).split('.')[0] + '.tex'
    with open(file_name, 'w') as f:
        f.write(s)
    os.system('latexmk -pdf -lualatex {0}'.format(file_name))
    os.system('latexmk -c {0}'.format(file_name))
