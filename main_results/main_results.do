/*
Main results
*/

local ver = "s13o2"

local ivs  = "AUDITOR_SEARCH_DUMMY AUDITOR_SEARCH_LOG1 AUDITOR_SEARCH_RANK"
local ivs_n : word count `ivs'

local count 0

forvalues i=1/`ivs_n'{

   local count_str = "000`count'"
   local len_e : length local count_str
   local len_s = `len_e' - 2
   local count_str = substr("`count_str'", `len_s', 3)

   import sas using "${rwp1_data_loc}/`ver'd1.sas7bdat", case(upper) clear

   local dvs="ABS_JONES ABS_MJONES"
   local dvs_n : word count `dvs'
   local ctl="NATIONAL_TENURE LOG_SIZE LEVERAGE ROA ROA_LAG1 LOSS CFO BOOK_TO_MARKET ALTMANZ SALE_GROWTH  LOG_CFOV RESTRUCTURING MA PAYABLES_TURNOVER_LAG1 RECEIVABLES_TURNOVER_LAG1"

   qui: eststo clear

   local ivs_i : word `i' of `ivs'

   forvalues j=1/`dvs_n'{
      local dvs_j : word `j' of `dvs'
      qui: reghdfe `dvs_j' `ivs_i' `ctl' ///
        , absorb(GVKEY_CGVKEY YEAR) ///
        cluster(GVKEY_CGVKEY)
      qui: eststo
      qui: estadd scalar nobs=_N
      qui: estadd scalar r2ap=e(r2_a)
      qui: estadd local fe "Yes"
      qui: estadd local cluster "Yes"
      }

   import sas using "${rwp1_data_loc}/`ver'd2.sas7bdat", case(upper) clear

   egen G = group(GVKEY_CGVKEY)

   local dvs="RESTATES"
   local dvs_n : word count `dvs'
   local ctl="NATIONAL_TENURE LOG_SIZE LEVERAGE ROA ROA_LAG1 LOSS CFO BOOK_TO_MARKET ALTMANZ SALE_GROWTH  LOG_CFOV RESTRUCTURING MA PAYABLES_TURNOVER_LAG1 RECEIVABLES_TURNOVER_LAG1"

   local ivs_i : word `i' of `ivs'

   forvalues j=1/`dvs_n'{
      local dvs_j : word `j' of `dvs'
      qui: clogit `dvs_j' `ivs_i' `ctl' i.FYEAR ///
        , group(GVKEY_CGVKEY)
      qui: predict phat if e(sample)
      qui: logit `dvs_j' `ivs_i' `ctl' i.FYEAR ///
        i.G if phat!=., cluster(GVKEY_CGVKEY)
      qui: eststo
      qui: estadd scalar nobs=_N
      qui: estadd scalar r2ap=e(r2_p)
      qui: estadd local fe "Yes"
      qui: estadd local cluster "Yes"
      }

   import sas using "${rwp1_data_loc}/`ver'd4.sas7bdat", case(upper) clear

   egen G = group(GVKEY_CGVKEY)

   local dvs="MEET_OR_BEAT"
   local dvs_n : word count `dvs'
   local ctl="NATIONAL_TENURE LOG_SIZE LEVERAGE ROA ROA_LAG1 LOSS CFO BOOK_TO_MARKET ALTMANZ SALE_GROWTH LOG_CFOV RESTRUCTURING MA PAYABLES_TURNOVER_LAG1 RECEIVABLES_TURNOVER_LAG1 NUMEST_LOG FORECAST_DISPERSION"

   forvalues j=1/`dvs_n'{
      local dvs_j : word `j' of `dvs'
      qui: clogit `dvs_j' `ivs_i' `ctl' i.FYEAR ///
        , group(GVKEY_CGVKEY)
      qui: predict phat if e(sample)
      qui: logit `dvs_j' `ivs_i' `ctl' i.FYEAR ///
        i.G if phat!=., cluster(GVKEY_CGVKEY)
      qui: eststo
      qui: estadd scalar nobs=_N
      qui: estadd scalar r2ap=e(r2_p)
      qui: estadd local fe "Yes"
      qui: estadd local cluster "Yes"
      }

   esttab using  ///
     $rwp1_folder_loc/main_results/reg_table_`count_str'.tex ///
     , booktabs unstack nobaselevels replace ///
     prehead("\clearpage" ///
     "{" ///
     "\sisetup{" ///
     "table-space-text-pre={(}," ///
     "table-space-text-post={$^{***}$}," ///
     "}" ///
     "\begin{center}" ///
     "\begin{longtable}" ///
     "{>{\kern-\tabcolsep}l" ///
     "*{@M} {S[table-format=-3.3, " ///
     "table-column-width=.75in]}" ///
     "<{\kern-\tabcolsep}}" ///
     "\caption{Main Tests} \\" ///
     "\toprule" ///
     "& {ABS_JONES_ATMCL1}" ///
     "& {ABS_MJONES_ATMCL1}" ///
     "& {RESTATS_DUMMY}" ///
     "& {MEET_OR_BEAT_MEDIAN}" ///
     "\\ \midrule" ///
     "\endfirsthead" ///
     "\multicolumn{@span}" ///
     "{>{\kern-\tabcolsep}c" ///
     "<{\kern-\tabcolsep}}" ///
     "{\textbf{\tablename\" ///
     "\thetable{}:} (Continued)}" ///
     "\\ \midrule" ///
     "& {ABS_JONES_ATMCL1}" ///
     "& {ABS_MJONES_ATMCL1}" ///
     "& {RESTATS_DUMMY}" ///
     "& {MEET_OR_BEAT_MEDIAN}" ///
     "\\ \midrule" ///
     "\endhead" ///
     "\multicolumn{@span}" ///
     "{>{\kern-\tabcolsep}r" ///
     "<{\kern-\tabcolsep}}" ///
     "{(\textit{Continued on next page})}" ///
     "\\ \endfoot" ///
     "\bottomrule" ///
     "\endlastfoot" ///
     ) ///
     posthead("") ///
     prefoot("\midrule")  ///
     postfoot("\end{longtable}" ///
     "\end{center}" ///
     "{\setstretch{1} \footnotesize" ///
     "\input{tabnote_vardef.tex}" ///
     "\input{tabnote.tex}" ///
     "\par}" ///
     "}" ///
     ) ///
     drop(*.G *.FYEAR) ///
     varlabels(_cons "\textit{Intercept}") ///
     nogaps b(3) t(3) ///
     eqlabels(none) collabels(none) nonumbers mlabels(none) ///
     stats(nobs r2 r2ap fe cluster, ///
     fmt(%8.0g %8.3f %8.3f %~12s %~12s) ///
     labels("Observations" "$ R^2 $" "Adj. / Pseudo $ R^2 $" "Include firm and year dummy" "Cluster by firm") ///
     layout("{@}" "{@}" "{@}" "{@}" "{@}") ///
     ) ///
     star(* 0.1 ** 0.05 *** 0.01)

   local count = `count' + 1

   }

cd $rwp1_folder_loc/main_results/
python script main_results.py
cd `:environment USERPROFILE'/Documents
