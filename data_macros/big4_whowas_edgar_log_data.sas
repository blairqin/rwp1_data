﻿/*
EDGAR log data with search made by Big4 auditors

Big4 auditors IP is based on ip_big4_data_whowas

NEED TO SPECIFY &KEY FIRST !!!
*/

%macro big4_whowas_edgar(
    ip_data = rwp1.ip_big4_data_whowas
    , dsetin =
    , dsetout =
    , debug = %str(*)
    )
    ;
    &debug OPTIONS MPRINT MLOGIC SYMBOLGEN;

    data _tmp_big4_edgar_log_data(drop=rc _r);
        if _n_=1 then do;
            if 0 then set &ip_data;
            dcl hash h(dataset:"&ip_data", multidata: "Y");
            h.definekey('ip1', 'ip2', 'ip3', 'ip4');
            h.definedata('auditor_fkey', 'id', 'country', 'date_s', 'date_e');
            h.definedone();
            call missing(auditor_fkey, id, country, date_s, date_e);
            end;
        set &dsetin(encryptkey=&key);
        rc = h.find();
        _r = 0;
        if rc=0 then do;
            if date_s<=date<=date_e then output;
            h.has_next(result: _r);
            do while(_r);
                rc = h.find_next();
                if rc=0 and date_s<=date<=date_e then output;
                h.has_next(result: _r);
                end;
            end;
    run;

    proc append base=&dsetout
        data=_tmp_big4_edgar_log_data;
    run;

    %check(_tmp_big4_edgar_log_data);

    &debug OPTIONS NOMPRINT NOMLOGIC NOSYMBOLGEN;
    %mend big4_whowas_edgar;


%let pwd = %get_pwd;
%let lib = rwp1;
libname &lib v9 "&pwd.data" outencoding=utf8;
libname data 'f:\EDGAR Log\ip_separated_data\';

ods exclude all;
proc contents data=data._all_ memtype=data nods;
    ods output Members = alldata(where=(substr(name, 1, 3)='LOG'));
run;
ods exclude none;

data _tmp1(keep=ip1-ip4 auditor_fkey id country date_s date_e);
    set rwp1.ip_big4_data_whowas;
run;

data _null_;
    set alldata;
    call symputx('tmp', name);
    call execute('%big4_whowas_edgar(ip_data=_tmp1, dsetin=data.&tmp, dsetout=big4_whowas_edgar_log_data)');
run;

%copy_data(
    dsetin    = big4_whowas_edgar_log_data
    , dsetout = rwp1.big4_whowas_edgar_log_data
    );

%check(big4_whowas_edgar_log_data _tmp1);
