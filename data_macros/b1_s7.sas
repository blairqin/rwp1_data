/*
Based on s7, a new branch, using the largest customer
*/

data _tmp_rwp1_data;
    set rwp1.s7;
run;

%means_by(
    dsetin             = _tmp_rwp1_data
    , by               = gvkey srcdate
    , allow_missing_by = N
    , variables        = salecs
    , method           = max
    , new_names        = salecs_max
    , allow_hash       = N
);

data _tmp_rwp1_data;
    set _tmp_rwp1_data(
        where=(salecs = salecs_max));
run;
