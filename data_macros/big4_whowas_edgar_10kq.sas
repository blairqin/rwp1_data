/*
Big4 search for 10KQ files
Using Big 4 EDGAR search validated by WhoWAS
*/

%let pwd = %get_pwd;
%let name = %get_name;
%let lib = rwp1;
libname &lib v9 "&pwd.data" outencoding=utf8;

data _tmp_edgar_index_10kq(drop=filename);
    set rwp1.edgar_index(
        where=(
        index(form_type, '10-K')   ~= 0
        or index(form_type, '10-Q')~= 0
        or index(form_type, '10K') ~= 0
        or index(form_type, '10Q') ~= 0
        )
    );
proc sort data=_tmp_edgar_index_10kq nodupkey;
    by cik accession;
data _tmp_big4_edgar_log_data;
    set rwp1.big4_whowas_edgar_log_data;
run;

%merge_left_join_hash(
    left_table            = _tmp_big4_edgar_log_data
    , left_table_index    = cik accession
    , right_table         = _tmp_edgar_index_10kq
    , right_table_index   = cik accession
    , allow_missing_index = N
    , variables           = form_type date_filed
    , new_names           =
);

data _tmp_big4_edgar_log_data;
    set _tmp_big4_edgar_log_data;
    where not missing(form_type);
run;

%copy_data(
    dsetin    = _tmp_big4_edgar_log_data
    , dsetout = &lib..&name
);

%check(_tmp_big4_edgar_log_data _tmp_edgar_index_10kq);
