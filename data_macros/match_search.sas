/*
Match auditor search
Merge auditor search into dsetin
dsetin: input dataset
cik: cik for input dataset
auditor: auditor key for input dataset
date: earnings announcement date for input dataset
range_start: start of the range, e.g., -365 or the variable of range start
range_end: end of the range, e.g., 0 or the variable of range end
aud_search_by_day: auditor search by day dataset
new_name: name the of auditor search variable
dsetout: output dataset

Usage

%match_search(
    dsetin              =
    , cik               =
    , auditor           =
    , date              =
    , range_start       =
    , range_end         =
    , aud_search_by_day =
    , search_vars       = count
    , new_name          =
);
*/

%macro match_search(
    dsetin              =
    , cik               =
    , auditor           =
    , date              =
    , range_start       =
    , range_end         =
    , aud_search_by_day =
    , search_vars       = count
    , new_name          =
    )/DES='Match auditor search'
    ;

    %local check;
    %let check = %sysfunc(prxmatch(/[a-zA-Z_]/, &range_start));

    data _tmp_match_search;
        set &dsetin(
            %if &check>0 %then %do;
            keep=&cik &auditor &date &range_start &range_end
            %end;
        %else %do;
            keep=&cik &auditor &date
            %end;
            );
            if cmiss(of _all_)=0;
    proc sort data=_tmp_match_search nodupkey;
        by &cik &auditor &date;
    data _tmp_match_search;
        set _tmp_match_search;
        do i=&range_start to &range_end;
            %if &check>0 %then %do;
                _tmp_date = i;
                %end;
            %else %do;
                _tmp_date = &date + i;
                %end;
            output;
            end;
    run;

    %merge_left_join_hash(
        left_table            = _tmp_match_search
        , left_table_index    = &cik &auditor _tmp_date
        , right_table         = &aud_search_by_day
        , right_table_index   = cik auditor_fkey date
        , allow_missing_index = N
        , variables           = &search_vars
        );

    %means(
        dsetin             = _tmp_match_search
        , by               = &cik &auditor &date
        , allow_missing_by = N
        , variables        = &search_vars
        , method           = sum
        , new_names        = &new_name
        , mean_out         = _tmp_match_search
        );

    %merge_left_join(
        left_table            = &dsetin
        , left_table_index    = &cik &auditor &date
        , right_table         = _tmp_match_search
        , right_table_index   = &cik &auditor &date
        , allow_missing_index = N
        , variables           = &new_name
        );

    %check(_tmp_match_search);

    %mend match_search;
