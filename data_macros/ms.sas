/*
Merge stuff jobs
*/

%macro ms(id);
    %local i;
    data _null_;
        set &lib..parameters(
            where=(type = "M"
            and id = "&id"
            )
        );
        call symputx(MARNAME, VALUE, 'l');
    run;
    %do i=1 %to &&&id._COUNT;
        %merge_left_join(
            left_table            = _tmp_rwp1_data
            , left_table_index    = &&&id._BY_&i
            , right_table         = &&&id._TAB_&i
            , right_table_index   = &&&id._BY_&i._NEW
            , allow_missing_index = N
            , variables           = &&&id._VARS_&i
            , new_names           = &&&id._VARS_&i._NEW
        );
    %end;
%mend ms;
