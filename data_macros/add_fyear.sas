/*
 Add fyear (fiscal year)
 */
%macro add_fyear(
    dsetin=
    , date_var=
    , fyear=
    )/DES='Add fiscal year'
    ;
    data &dsetin;
        set &dsetin;
        if not missing(&date_var) then do;
            if month(&date_var)<=5 then &fyear = year(&date_var) - 1;
            else &fyear = year(&date_var);
            end;
        else call missing(&fyear);
    run;
%mend add_fyear;
