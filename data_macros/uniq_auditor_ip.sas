﻿/* Get unique auditor ip range */

data auditor_ip;
    attrib auditor_fkey length=8
        start_address length=$15
        end_address length=$15
    ;
    set db.auditor_domain_ip(
    keep=auditor_fkey start_address end_address
    where=(length(start_address)<=15
        and length(end_address)<=15)
    );
proc sort data=auditor_ip nodupkey;
    by _all_;
    run;

/* Generate ip info */
data audip_ip14_0218;
    attrib ip1 length=8
        ip2 length=8
        ip3 length=8
        ip4 length=8
        auditor_fkey length=8
        ;
    infile "&rwp1_datapath.auditor_ip_match.txt"
    DSD DLM='09'x MISSOVER FIRSTOBS=2;
    input 
        ip1 :
        ip2 :
        ip3 :
        ip4 :
        auditor_fkey :
        ;
data audip_ip14_0218(drop=i);
    set audip_ip14_0218;
    if missing(ip3) then do;
        do i=0 to 255;
            ip3=i;
            output;
        end;
    end;
    else output;
data audip_ip14_0218(drop=i);
    set audip_ip14_0218;
    if missing(ip4) then do;
        do i=1 to 3;
        ip4 = i;
        output;
        end;
    end;
    else output;
    run;
/* 
    Drop auditor_fkey=2700 (Because wrong web_link)
    30212, 30447, 30623 (Because the same ip range)
*/
proc sort data=audip_ip14_0218(
    where=(auditor_fkey not in (2700, 30212, 30447, 30623))
);
    by ip1-ip4;
data audip_ip14_0218;
    set audip_ip14_0218;
    by ip1-ip4;
    if first.ip4=last.ip4=1;
    run;
proc datasets lib=rwp1 nolist;
    copy in=work out=rwp1 noclone;
    select audip_ip14_0218;
    modify audip_ip14_0218(
        write=&passwd
        alter=&passwd
    );
    ic create primary key (ip1 ip2 ip3 ip4);
    run;quit;
