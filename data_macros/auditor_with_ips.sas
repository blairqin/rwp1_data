/*
Get auditors with available IP address

audip_ip: data audip_ip
dsetout: output data
*/

%macro auditor_with_ips(
    audip_ip =
    , dsetout =
    , debug = %str(*)
    )/DES='Get auditors with available IPs'
    ;
    &debug OPTIONS MPRINT MLOGIC SYMBOLGEN;

    proc sort data=&audip_ip(keep=auditor_fkey)
        out=_tmp_auditor_with_ips
        nodupkey
        ;
        by auditor_fkey;
    data _tmp_auditor_with_ips;
        set _tmp_auditor_with_ips;
        available = 1;
    run;

    %copy_pk(
        dsetin    = _tmp_auditor_with_ips
        , dsetout = &dsetout
        , pk      = auditor_fkey
        );

    %check(_tmp_auditor_with_ips);

    &debug OPTIONS NOMPRINT NOMLOGIC NOSYMBOLGEN;
    %mend auditor_with_ips;

%auditor_with_ips(
    audip_ip  = rwp1.audip_ip14_0218
    , dsetout = rwp1.auditor_with_ips
);
