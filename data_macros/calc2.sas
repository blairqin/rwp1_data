/*
Calcualtion2 missing value to zero
*/

%macro calc2(id);
    %local
        i
        _var
        ;
    data _null_;
        set &lib..parameters(
            where=(type = "M"
            and id = "&id"
            )
        );
        call symputx(MARNAME, VALUE, 'l');
    data _tmp_rwp1_data;
        set _tmp_rwp1_data;
        %do i=1 %to %sysfunc(countw(&&&id._VAR));
            %let _var = %scan(&&&id._VAR, &i, %str( ));
            if missing(&_var) then &_var=0;
            else &_var=&_var;
        %end;
    run;
%mend calc2;
