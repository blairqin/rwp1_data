%let pwd = %get_pwd;
%let lib = rwp1;
libname &lib v9 "&pwd.data" outencoding=utf8;

/*
Add edgar_index data available from sec
*/

data tmp(drop=company_name compress=yes);
    attrib cik length=$10
        company_name length=$100
        form_type length=$20
        date_filed length=8
            format=yymmddn8.
        filename length=$50
        accession length=$30
    ;
    infile "&pwd./edgar_index.csv"
        dsd dlm=',' missover firstobs=2
    ;
    input
        cik :
        company_name :
        form_type :
        date_filed :yymmdd10.
        filename :
        accession :
    ;
run;

proc sort data=tmp nodupkey;
    by cik accession form_type;
run;

%copy_pk(
    dsetin    = tmp
    , dsetout = rwp1.edgar_index
    , pk      = cik accession form_type
);

%check(tmp);
