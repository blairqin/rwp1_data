/* rwp1 data */
/* Auditor information acquisition and audit quality */
/* Get data */

/* Stage 1 */

%include "./rwp1_data/stage1.sas";

/* Stag 2 Merge some variables, variables from Gipper et al 2020 TAR */

%include "./rwp1_data/stage2.sas";

/* Stage 3 Merge auditor information acquisition variables */

%include "./rwp1_data/stage3.sas";

/* Stage 4 Merge header information */

%include "./rwp1_data/stage4.sas";

/* Output based on stage 4 */

%include "./rwp1_data/stage4_out1.sas";

/* Stage4 Branch 1: Keep only the largest customer */

%include "./rwp1_data/stage4_b1.sas";
%include "./rwp1_data/stage4_b1_o1.sas";

/* Stage4 Branch 2: Using the average number of information search */

%include "./rwp1_data/stage4_b2.sas";
%include "./rwp1_data/stage4_b2_o1.sas";
