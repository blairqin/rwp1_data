%macro samp_sele_get_data(
    paras=
    , group=
    );

    %read_paras(infile=&paras, dsetout=paras);
    data paras;
        set paras(
        where=(group="&group")
        );
        call symputx(paras, value, 'l');
    run;

    data &samp_data;
        set &indata;
    run;

    %check(&samp_data_log)

    %samp_sele_log(
        data=&samp_data
        , procedure=&procedure
        , dsetout=&samp_data_log
        );

    %mend samp_sele_get_data;

%macro samp_sele_period(
    paras=
    , group=
    )/DES='Specific to a sample period'
    ;

    %read_paras(infile=&paras, dsetout=paras);
    data paras;
        set paras(
        where=(group="&group")
        );
        call symputx(paras, value, 'l');
    run;

    data &samp_data;
        set &samp_data(
            where=(&start_date
            <=&datevar
            <=&end_date)
            );
    run;

    %samp_sele_log(
        data=&samp_data
        , procedure=&procedure
        , dsetout=&samp_data_log
        );

%mend samp_sele_period;

%macro samp_sele_nomiss(
    paras=
    , group=
    )/DES='Remove missing variables'
    ;

    %read_paras(infile=&paras, dsetout=paras);
    data paras;
        set paras(
        where=(group="&group")
        );
        call symputx(paras, value, 'l');
    run;

    %local _nomiss_comma;
    %let _nomiss_comma = %sysfunc(prxchange(s/(\s+)/%str(,)/, -1, &nomiss));

    data &samp_data;
        set &samp_data(
            where=(cmiss(&_nomiss_comma)=0)
            );
    run;

    %samp_sele_log(
        data=&samp_data
        , procedure=&procedure
        , dsetout=&samp_data_log
        );

    %mend samp_sele_nomiss;
