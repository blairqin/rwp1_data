/*
Based on match_search macro
Allow to use most recent search
range_start and range_end also applies to filing_date
Match auditor search
Merge auditor search into dsetin
dsetin: input dataset
cik: cik for input dataset
auditor: auditor key for input dataset
date: earnings announcement date for input dataset
range_start: start of the range, e.g., -365 or the variable of range start
range_end: end of the range, e.g., 0 or the variable of range end
aud_search_by_day: auditor search by day dataset
new_name: name the of auditor search variable
dsetout: output dataset

Usage

%match_search_mostrc(
    dsetin              =
    , cik               =
    , auditor           =
    , date              =
    , range_start       =
    , range_end         =
    , aud_search_by_day =
    , search_vars       = count
    , new_name          =
);
*/

%macro match_search_mostrc(
    dsetin              =
    , cik               =
    , auditor           =
    , date              =
    , range_start       =
    , range_end         =
    , aud_search_by_day =
    , search_vars       = count
    , new_name          =
    )/DES='Match auditor search'
    ;

    data _tmp_match_search_mostrc;
        set &dsetin(
            keep=&cik &auditor &date
            );
            if cmiss(of _all_)=0;
    proc sort data=_tmp_match_search_mostrc nodupkey;
        by &cik &auditor &date;
    run;

    data _tmp_match_search_mostrc1;
        set &aud_search_by_day(
            keep=cik auditor_fkey date date_filed all_search accession
            );
        if cmiss(of _all_)=0;
    run;

    proc sql noprint undo_policy=none;
        create table _tmp_match_search_mostrc as
            select a.*, b.accession, b.date_filed, b.all_search
            from _tmp_match_search_mostrc a
            left join _tmp_match_search_mostrc1 b
            on a.&cik = b.cik
            and a.&auditor = b.auditor_fkey
            and &range_start <= b.date - a.&date <= &range_end
            and &range_start <= b.date_filed - a.&date <= &range_end
            ;
        quit;

    %means(
        dsetin             = _tmp_match_search_mostrc
        , by               = &cik &auditor &date
        , allow_missing_by = N
        , variables        = &search_vars
        , method           = sum
        , new_names        = &new_name
        , mean_out         = _tmp_match_search_mostrc
        );

    %merge_left_join(
        left_table            = &dsetin
        , left_table_index    = &cik &auditor &date
        , right_table         = _tmp_match_search_mostrc
        , right_table_index   = &cik &auditor &date
        , allow_missing_index = N
        , variables           = &new_name
        );

    %check(_tmp_match_search_mostrc
        _tmp_match_search_mostrc1);

    %mend match_search_mostrc;
