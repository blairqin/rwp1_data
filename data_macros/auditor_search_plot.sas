/*
 Macro for auditor search
 */
%macro auditor_search_plot(
    paras=
    , group=
    )
    ;
    %read_paras(
        infile=&paras
        , dsetout=_paras
        );
    data _paras;
        set _paras(
        where=(group="&group")
        );
        call symputx(paras, value, 'l');
    run;

    data _auditor_search_plot;
        set &dsetin;
    run;

    %local _format_no i _format_curr _method_curr;
    %let _format_no = %sysfunc(countw(&format, ' '));

    %do i=1 %to &_format_no;
        %let _format_curr = %scan(&format, &i, ' ');
        %let _method_curr = %scan(&method, &i, ' ');
        proc means
            data=_auditor_search_plot
            noprint
            order=formatted
            ;
            class &date;
            types &date;
            format &date &_format_curr;
            var &count;
            output out=_auditor_search_plot
                &_method_curr(&count)=&count;
        run;
        %end;

    data &dsetout(
        drop = &date
        rename = (_tmpvar = &date)
        write = &passwd
        alter = &passwd
        );
        set _auditor_search_plot;
        _tmpvar = put(&date, &plot_format);
    run;

    %check(_paras _auditor_search_plot);
%mend auditor_search_plot;
