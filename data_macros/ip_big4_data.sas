﻿/*
Only Big4 IPs for the matching purpose
These IPs have been verified by Mia
*/

%let pwd = %get_pwd;
%let lib = rwp1;
libname &lib v9 "&pwd.data" outencoding=utf8;

data _tmp_ip_big4_data;
    attrib
        auditor_fkey length=8
        ip_s length=$15
        ip_e length=$15
        date_s format=yymmddn8.
        date_e format=yymmddn8.
        net_handle length=$25
        classification length=8
        name length=$50
        ;
    infile "&pwd.data/ip_big4_data.csv"
        delimiter=',' dsd missover;
    input
        auditor_fkey :
        ip_s :
        ip_e :
        date_s :mmddyy10.
        date_e :mmddyy10.
        net_handle :
        classification :
        name:
        ;
data _tmp_ip_big4_data;
    set _tmp_ip_big4_data;
    ip_s_1 = input(scan(ip_s, 1, '.'), best12.);
    ip_s_2 = input(scan(ip_s, 2, '.'), best12.);
    ip_s_3 = input(scan(ip_s, 3, '.'), best12.);
    ip_s_4 = length(scan(ip_s, 4, '.'));
    ip_e_1 = input(scan(ip_e, 1, '.'), best12.);
    ip_e_2 = input(scan(ip_e, 2, '.'), best12.);
    ip_e_3 = input(scan(ip_e, 3, '.'), best12.);
    ip_e_4 = length(scan(ip_e, 4, '.'));
data _tmp_ip_big4_data(
    keep=auditor_fkey ip1-ip4
    date_s date_e net_handle
    classification name
    );
    set _tmp_ip_big4_data;
    do i=ip_s_1 to ip_e_1;
        do j=ip_s_2 to ip_e_2;
            do k=ip_s_3 to ip_e_3;
                do l=ip_s_4 to ip_e_4;
                    ip1=i;
                    ip2=j;
                    ip3=k;
                    ip4=l;
                    output;
                    end;
                end;
            end;
        end;
run;

%copy_pk(
    dsetin    = _tmp_ip_big4_data
    , dsetout = rwp1.ip_big4_data
    , pk      = ip1 ip2 ip3 ip4
    );

%check(_tmp_ip_big4_data);
