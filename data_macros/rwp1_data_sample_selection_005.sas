* rwp1 data sample selection based on &SMP_NAME_STAGE_4;
* Auditor information acquisition and audit quality;
%let pwd = %get_pwd;
%let lib = rwp1;
%let paras_csv = parameters.csv;
libname &lib v9 "&pwd.data" outencoding=utf8;

%clear_log;
%read_paras(infile=&pwd&paras_csv, dsetout=&lib..parameters);
data _null_;
    set &lib..parameters(
        where=(type = "D")
    );
    call symputx(MARNAME, VALUE, 'g');
run;

* Input data;

data _rwp1_data_sample_selection;
    set &SMP_NAME_STAGE_5;
run;
%samp_sele_log(
    data        = _rwp1_data_sample_selection
    , procedure = Supplier-customer data from Compustat (&SMP_NAME_STAGE_5)
    , dsetout   = _rwp1_data_sample_selection_log
);

* Stage1;

data _rwp1_data_sample_selection;
    set _rwp1_data_sample_selection;
    where 2003<=s_fyear<=2016;
run;
%samp_sele_log(
    data        = _rwp1_data_sample_selection
    , procedure = Observations that fall between 2003 to 2016
    , dsetout   = _rwp1_data_sample_selection_log
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection
    , dsetout = &SS5_STAGE_1
    , pk      = s_gvkey c_gvkey s_datadate
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection_log
    , dsetout = &SS5_STAGE_LOG_1
    , pk      = nobs
);

* Stage2;

data _rwp1_data_sample_selection;
    set _rwp1_data_sample_selection;
    where 1<=s_AUDITOR_FKEY<=4;
run;
%samp_sele_log(
    data        = _rwp1_data_sample_selection
    , procedure = Keep supplier audit firms audited by big4 auditors
    , dsetout   = _rwp1_data_sample_selection_log
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection
    , dsetout = &SS5_STAGE_2
    , pk      = s_gvkey c_gvkey s_datadate
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection_log
    , dsetout = &SS5_STAGE_LOG_2
    , pk      = nobs
);

* Stage3;

%remove_sic(
    dsetin      = _rwp1_data_sample_selection
    , sic       = s_sic2
    , sic_digit = 2
    , start_sic = 60
    , end_sic   = 69
    , dsetout   = _rwp1_data_sample_selection
);
%samp_sele_log(
    data        = _rwp1_data_sample_selection
    , procedure = Remove firms in the financial industry (sic from 60 to 69)
    , dsetout   = _rwp1_data_sample_selection_log
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection
    , dsetout = &SS5_STAGE_3
    , pk      = s_gvkey c_gvkey s_datadate
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection_log
    , dsetout = &SS5_STAGE_LOG_3
    , pk      = nobs
);

* Stage4;

%remove_sic(
    dsetin      = _rwp1_data_sample_selection
    , sic       = s_sic2
    , sic_digit = 2
    , start_sic = 44
    , end_sic   = 49
    , dsetout   = _rwp1_data_sample_selection
);
%samp_sele_log(
    data        = _rwp1_data_sample_selection
    , procedure = Remove firms in the regulated industry (sic from 44 to 49)
    , dsetout   = _rwp1_data_sample_selection_log
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection
    , dsetout = &SS5_STAGE_4
    , pk      = s_gvkey c_gvkey s_datadate
);
%copy_pk(
    dsetin    = _rwp1_data_sample_selection_log
    , dsetout = &SS5_STAGE_LOG_4
    , pk      = nobs
);

%check(_rwp1_data_sample_selection
    _rwp1_data_sample_selection_log
    );
