/*
Separate auditor data IPs into four blocks

! NEED TO SPECIFY &KEY FIRST !!!
+ Block1 to block3 is actual IP octlet
+ Block4 is the number of digits of the fourth IP octlet
*/

%macro auditor_log_sep_ip(
    dsetin =
    , dsetout_lib =
    , debug = %str(*)
    )/DES='Separate IPs'
    ;
    &debug OPTIONS MPRINT MLOGIC SYMBOLGEN;

    %local dta;
    %let dta = %get_data(&dsetin);
    %put PROCESSING &dsetin INTO &dsetout_lib..&dta;

    data &dsetout_lib..&dta(encrypt=aes encryptkey=&key);
        set &dsetin(encryptkey=&key);
        if not missing(ip) then do;
            ip1 = input(scan(ip, 1, '.'), best12.);
            ip2 = input(scan(ip, 2, '.'), best12.);
            ip3 = input(scan(ip, 3, '.'), best12.);
            ip4 = length(scan(ip, 4, '.'));
            end;
        else call missing(ip1, ip2, ip3, ip4);
    run;

    &debug OPTIONS NOMPRINT NOMLOGIC NOSYMBOLGEN;
    %mend auditor_log_sep_ip;

libname dtaout "f:\EDGAR Log\ip_separated_data\";
libname dtain  "f:\EDGAR Log\sasdata\";

ods exclude all;
proc contents data=dtain._all_ memtype=data nods;
    ods output Members = alldata(where=(substr(name, 1, 3)='LOG'));
run;
ods exclude none;


data _null_;
    set alldata;
    call symputx('tmp', name);
    call execute('%auditor_log_sep_ip(dsetin=dtain.&tmp, dsetout_lib=dtaout)');
run;
