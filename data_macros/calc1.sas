* Some calculations;
* Clause value is not missing, vars are separated by space;
* Clause value new is additional clause;
* Calc value is new variable name;
* Calc value new is the calculation;

%macro calc1(id);
    %local
        i
        ;
    data _null_;
        set &lib..parameters(
            where=(type = "M"
            and id = "&id"
            )
        );
        call symputx(MARNAME, VALUE, 'l');
    data _tmp_rwp1_data;
        set _tmp_rwp1_data;
        %do i=1 %to &&&id._CLAUSE_NO;
            if cmiss(of &&&id._CLAUSE&i)=0
            %if %symexist(&id._CLAUSE&i._NEW)=1 %then %str(and &&&id._CLAUSE&i._NEW);
            then &&&id._CALC&i=&&&id._CALC&i._NEW;
            else call missing(&&&id._CALC&i);
        %end;
    run;
%mend calc1;
