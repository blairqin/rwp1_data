/*
EDGAR based on big4_edgar_log_data_10kq
1. Number of information acquisition search by auditor date and cik
*/

%let pwd = %get_pwd;
%let lib = rwp1;
libname &lib v9 "&pwd.data" outencoding=utf8;


data _tmp_big4_edgar_log_data_by_adc(
    drop=cik
    rename=(ccik=cik)
    );
    set rwp1.big4_edgar_log_data_10kq;
    attrib ccik length=$10;
    ccik = put(input(cik, best12.), z10.);
run;

%freq(
    dsetin             = _tmp_big4_edgar_log_data_by_adc
    , by               = auditor_fkey date cik
    , allow_missing_by = N
    , new_names        = COUNT
    , freq_out         = _tmp_big4_edgar_log_data_by_adc
    );

%copy_pk(
    dsetin    = _tmp_big4_edgar_log_data_by_adc
    , dsetout = rwp1.big4_edgar_log_data_10kq_by_adc
    , pk      = auditor_fkey date cik
    );

%check(_tmp_big4_edgar_log_data_by_adc);
