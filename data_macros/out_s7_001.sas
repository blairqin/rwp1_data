* rwp1 data output based on stage7 data;
* Auditor information acquisition and audit quality;
%let pwd = %get_pwd;
%let lib = rwp1;
%let paras_csv = parameters.csv;
libname &lib v9 "&pwd.data" outencoding=utf8;

%clear_log;
%read_paras(infile=&pwd&paras_csv, dsetout=&lib..parameters);

%outputg2_paras(
    ids          = out_s7_1
    , parameters = rwp1.parameters
);
