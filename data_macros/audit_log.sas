﻿/*
Merge logfile with auditor ips
*/

proc printto log="&rwp1_datapath.audit_log.log" new;run;
libname edgar 'f:\EDGAR Log\sasdata\';

ods exclude all;
proc contents data=edgar._all_ nods;
ods output Members=tabout;
run;
ods exclude none;

%macro big4(
    libin=
    , dsetin=
    , libout=
    , dsetout=);
    data &libout..&dsetout;
        set &libin..&dsetin(encryptkey=&key);
        ip1=input(scan(ip, 1, '.') ,best12.);
        ip2=input(scan(ip, 2, '.') ,best12.);
        ip3=input(scan(ip, 3, '.') ,best12.);
        ip4=length(scan(ip, 4, '.'));
    data &libout..&dsetout(
        drop=ip1-ip4
        write=&passwd
        alter=&passwd
    );
        if _n_=1 then do;
        if 0 then set rwp1.audip_ip14_0218;
        dcl hash h(dataset: 'rwp1.audip_ip14_0218');
        h.definekey('ip1', 'ip2', 'ip3', 'ip4');
        h.definedata('auditor_fkey');
        h.definedone();
        end;
        set &libout..&dsetout;
        if h.find()=0;
        run;
%mend big4;

data tabout;
    set tabout;
    call symputx('dsetin', name);
    call execute('%big4(libin=edgar
        , dsetin=&dsetin
        , libout=rwp1
        , dsetout=&dsetin)');
    run;
proc printto;run;

/* Combine together */

proc printto log="&rwp1_datapath.audit_log_combine.log" new;run;
ods exclude all;
proc contents data=rwp1._all_ memtype=data nods;
ods output Members=alldata(where=(substr(name, 1, 3)='LOG'));
run;
ods exclude none;

%macro append(libin=rwp1, dsetin=);
    proc append base=aud_log data=&libin..&dsetin;run;
%mend append;

data alldata;
    set alldata;
    call symputx('dsetin', name);
    call execute('%append(dsetin=&dsetin)');
    run;
proc printto;run;

/* Add leading 0 to cik */
data aud_log(drop=cik
    rename=(ccik=cik));
    set aud_log;
    attrib ccik length=$10;
    ccik = put(input(cik, best12.), z10.);
    run;

proc datasets lib=rwp1 nolist;
    copy in=work out=rwp1 noclone;
    select aud_log;
    modify aud_log(write=&passwd alter=&passwd);
    run;quit;
