﻿data rwp1.auddomain_afk_0218;
    attrib auditor_fkey length=8
    domain length=$50
    ;
    infile "&rwp1_datapath.auddomain_afk_0218.txt"
    dsd missover dlm='09'X firstobs=2
    ;
    input
        auditor_fkey
        domain
    ;
proc sort data=rwp1.auddomain_afk_0218
    out=rwp1.auddomain_afk_0218(
        alter=&passwd
        write=&passwd
    );
    by auditor_fkey;
    run;
