﻿%macro stcodes(stcode);
    %if CA=%bquote(&stcode) %then %put &=stcode: State is California;
    %else %put &=stcode: State is NOT California;
%mend stcodes;

%stcodes(OR);
