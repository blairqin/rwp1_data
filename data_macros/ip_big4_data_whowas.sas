/*
Import IP data with validated from WhoWAS
*/

%let pwd = %get_pwd;
%let lib = rwp1;
libname &lib v9 "&pwd.data" outencoding=utf8;

data _tmp_ip_big4_data_whowas;
    attrib
        id length=$15
        auditor_fkey length=8
        ip_s length=$15
        ip_e length=$15
        date_s format=yymmddn8.
        date_e format=yymmddn8.
        name length=$80
        country length=$3
        ;
    infile "&pwd.data/ip_big4_data_whowas.csv"
        delimiter=',' dsd missover
        firstobs=2;
    input
        auditor_fkey :
        ip_s :
        ip_e :
        date_s :mmddyy10.
        date_e :mmddyy10.
        name :
        country :
        ;
data _tmp_ip_big4_data_whowas;
    set _tmp_ip_big4_data_whowas;
    id     = tranwrd(ip_s, '.', '_');
    ip_s_1 = input(scan(ip_s, 1, '.'), best12.);
    ip_s_2 = input(scan(ip_s, 2, '.'), best12.);
    ip_s_3 = input(scan(ip_s, 3, '.'), best12.);
    ip_s_4 = length(scan(ip_s, 4, '.'));
    ip_e_1 = input(scan(ip_e, 1, '.'), best12.);
    ip_e_2 = input(scan(ip_e, 2, '.'), best12.);
    ip_e_3 = input(scan(ip_e, 3, '.'), best12.);
    ip_e_4 = length(scan(ip_e, 4, '.'));
    if missing(country) then country = 'US';
    if missing(date_e) then date_e = '09SEP2020'd;
data _tmp_ip_big4_data_whowas(
    keep= id auditor_fkey ip1-ip4
    date_s date_e
    name
    country
    );
    set _tmp_ip_big4_data_whowas;
    do i=ip_s_1 to ip_e_1;
        do j=ip_s_2 to ip_e_2;
            do k=ip_s_3 to ip_e_3;
                do l=ip_s_4 to ip_e_4;
                    ip1=i;
                    ip2=j;
                    ip3=k;
                    ip4=l;
                    output;
                    end;
                end;
            end;
        end;
run;

%copy_pk(
    dsetin    = _tmp_ip_big4_data_whowas
    , dsetout = rwp1.ip_big4_data_whowas
    , pk      = ip1 ip2 ip3 ip4 date_s
    );

%check(_tmp_ip_big4_data_whowas);
