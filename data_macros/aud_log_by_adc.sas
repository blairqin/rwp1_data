/*
Auditor log by auditor, date, cik

Usage

%aud_log_by_adc(
    dsetin    =
    , dsetout =
);
*/

%macro aud_log_by_adc(
    dsetin =
    , dsetout =
    , debug = %str(*)
    )/DES='Auditor log by day'
    store
    ;
    &debug OPTIONS MPRINT MLOGIC SYMBOLGEN;

    %freq(
        dsetin             = &dsetin
        , by               = auditor_fkey date cik
        , allow_missing_by = N
        , new_names        = count
        , freq_out         = _tmp_aud_log_by_adc
        );

    %copy_pk(
        dsetin    = _tmp_aud_log_by_adc
        , dsetout = &dsetout
        , pk      = auditor_fkey date cik
        );

    %check(_tmp_aud_log_by_adc);

    &debug OPTIONS NOMPRINT NOMLOGIC NOSYMBOLGEN;
    %mend aud_log_by_adc;

%aud_log_by_adc(
    dsetin    = rwp1.aud_log
    , dsetout = rwp1.aud_log_by_adc
);
