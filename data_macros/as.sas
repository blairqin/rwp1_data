/*
Auditor search (as) macro

Usage

%as(
ids         =
, para_data =
);
*/

%macro as(
    ids =
    , para_data =
    );
    %local ids idc i;
    %let ids = %str_to_list(&ids);
    %let ids = %upcase(&ids);

    %do i=1 %to %sysfunc(countw(&ids, %str( )));
        %let idc = %scan(&ids, &i, %str( ));

        data _null_;
            set &para_data;
            if upcase(type)="M"
                and upcase(id)="&idc"
                then do;
                call symputx(MARNAME, VALUE, 'l');
                end;
        run;

        %local
            dsetin
            cik
            aud
            date
            rs
            re
            aud_sdta
            name
            name_d1
            name_l1
            name_l2
            ;

        %if %symexist(&idc._dsetin) =1 %then %let dsetin = &&&idc._dsetin;
        %else %let dsetin =;
        %if %symexist(&idc._cik)=1 %then %let cik = &&&idc._cik;
        %else %let cik =;
        %if %symexist(&idc._aud)=1 %then %let aud = &&&idc._aud;
        %else %let aud =;
        %if %symexist(&idc._date)=1 %then %let date = &&&idc._date;
        %else %let date =;
        %if %symexist(&idc._rs)=1 %then %let rs = &&&idc._rs;
        %else %let rs =;
        %if %symexist(&idc._re)=1 %then %let re = &&&idc._re;
        %else %let re =;
        %if %symexist(&idc._aud_sdta)=1 %then %let aud_sdta = &&&idc._aud_sdta;
        %else %let aud_sdta =;
        %if %symexist(&idc._name)=1 %then %let name = &&&idc._name;
        %else %let name =;
        %if %symexist(&idc._name_d1)=1 %then %let name_d1 = &&&idc._name_d1;
        %else %let name_d1 =;
        %if %symexist(&idc._name_l1)=1 %then %let name_l1 = &&&idc._name_l1;
        %else %let name_l1 =;
        %if %symexist(&idc._name_l2)=1 %then %let name_l2 = &&&idc._name_l2;
        %else %let name_l2 =;

        %match_search(
            dsetin              = &dsetin
            , cik               = &cik
            , auditor           = &aud
            , date              = &date
            , range_start       = &rs
            , range_end         = &re
            , aud_search_by_day = &aud_sdta
            , new_name          = &name
            , dsetout           = _tmp_match_search_as
            );

        %merge_left_join_hash(
            left_table            = &dsetin
            , left_table_index    = &cik &aud &date
            , right_table         = _tmp_match_search_as
            , right_table_index   = &cik &aud &date
            , allow_missing_index = N
            , variables           = &name
            );

        data &dsetin;
            set &dsetin;

            if missing(&name) then &name=0;
            else &name=&name;

            if &name>0 then &name_d1=1;
            else &name_d1=0;

            if &name>0 then &name_l1= log(&name);
            else &name_l1=0;

            if &name>0 then &name_l2 = log(1+&name);
            else &name_l2 = 0;
            label
                &name    = "&aud SEARCH &cik FROM &rs &date TO &re &date"
                &name_d1 = "DUMMY VAR SET TO ON IF &name>0 AND 0 OTHERWISE"
                &name_l1 = "LOG OF &name"
                &name_l2 = "LOG OF 1 PLUS &name"
                ;
        run;

        %end;

    %check(_tmp_match_search_as);

    %mend as;
