﻿* rwp1 data output based on stage3 data;
* Auditor information acquisition and audit quality;
%let pwd = %get_pwd;
%let lib = rwp1;
%let paras = parameters.xlsx;
%let paras_csv = parameters.csv;
libname &lib v9 "&pwd.data" outencoding=utf8;

options noxwait;
%sysexec python &pwd.parameters.py &paras;
%clear_log;
%read_paras(infile=&pwd&paras_csv, dsetout=&lib..parameters);

%outputg2_paras(
    ids          = out_s3_1 out_s3_2
    , parameters = rwp1.parameters
);
