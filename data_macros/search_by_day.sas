%macro search_by(
    dsetin=
    , search_by=
    , dsetout=
    )/DES='Number of search by &search_by'
    ;
    proc freq data=&dsetin noprint;
        table &search_by / out=&dsetout;
    run;
%mend search_by_day;
