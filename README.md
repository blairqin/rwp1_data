# RWP1 DATA

## Setup

1. Include`rwp1_profile.do` into STATA `profile.do`;
2. Modify `rwp1_folder_loc` and `rwp1_data_loc` to the folder location and data location respectively.
3. Setup the `rwp1_data` module by `pip install -e .`;

## Progress

### 📌 [Main results](./main_results/main_results.pdf)

1. Table and table notes: ✔
2. Variable definition table: ✔

### 📌 [Cross sectional tests based on relationship-specific investments (RSI)](cross_sectional_rsi/cross_sectional_rsi.pdf)

1. Table and table notes: ✔
2. Variable definition table: ✔

### 📌 [Cross sectional tests based on common auditor](cs_common_auditor/cs_common_auditor.pdf)

1. Table and table notes: ✔
2. Variable definition table: ✔

### 📌 [Robustness tests based on common auditor](rt_common_auditor/rt_common_auditor.pdf)

1. Table and table notes: ✔
2. Variable definition table: ✔