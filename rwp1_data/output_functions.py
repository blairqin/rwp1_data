import pandas as pd
import re
import io
import json
from . import rename

pd.options.display.max_colwidth = 5000

def parse_tab(df: pd.DataFrame() = None, file: str = None, d: dict = rename.rename) -> tuple:
    '''
    Process schema.table from con to latex format
    @param d: Dict for replace
    @param firstcol_to_it: bool True to add \textit{} for all values in the first col, only when df is not None
    @param df: pandas dataframe
    @param file: file path to be processed
    @return: tuple(LaTeX table, number of columns)
    '''
    if df is not None:
        f = io.StringIO()
        df.to_latex(f, index=False, longtable=False, header=True)
        s = f.getvalue()
        length = len(df.columns)
        p = re.compile(r'\\begin\{tabular\}.+?\\toprule\n', re.DOTALL)
        s = p.sub('', s)
        p = re.compile(r'\n\\bottomrule\n\\end{tabular}\n', re.DOTALL)
        s = p.sub('', s)
        p = re.compile(r'\\midrule', re.DOTALL)
        s = p.sub('', s)
    elif file is not None:
        with io.open(file, encoding='utf-8') as f:
            s = f.read()
        length = None
    s = s.replace('\\_', '_')
    s = s.replace('(.)', '')
    if d is not None:
        s = replace_names(s=s, d=d)
    p = re.compile(r'\\sym\{(\*+)\}', re.I)
    s = p.sub(r'$^{\g<1>}$', s)
    s = '\n'.join(x.strip() for x in s.partition('\n'))
    t = (s, length)
    return t


def add_row_color(t: tuple, rows_no: tuple, color: str = 'LightCyan'):
    '''
    Add row color
    :param t: tuple, (length, string)
    :param rows_no: tuple, a tuple of number that needed to add row color
    :param color: row color, default is LightCyan
    :return: tuple (length, string)
    '''
    length = t[1]
    s = ''
    for num, row in enumerate(t[0].split('\n')):
        if num in rows_no:
            s = s + r'\rowcolor{{{0}}} '.format(color) + row
        else:
            s = s + row
        s = s + '\n'
    s = '\n'.join(x.strip() for x in s.partition('\n'))
    t = (s, length)
    return t


def add_header_footer(t: tuple
                      , tbl_preamble: str = None
                      , col_fmt: str = None
                      , title: str = None
                      , landscape: bool = False) -> tuple:
    '''
    Add header or footer to table
    @param tbl_preamble: string before table
    @param t: tuple (string, number of cols)
    @param col_fmt: column format
    @param title: title of table
    @param landscape: bool, True to use landscape, default False
    @return:
    '''
    s = t[0]
    if tbl_preamble is not None:
        header_s = r'''{{
        \clearpage
        {0}
        \begin{{center}}
        '''.format(tbl_preamble)
    else:
        header_s = r'''{
        \clearpage
        \begin{center}
        '''
    if landscape is True:
        header_s = header_s + r'''
        \begin{{landscape}}
        \begin{{longtable}}
        {{{0}}}
        '''.format(col_fmt)
    else:
        header_s = header_s + r'''
        \begin{{longtable}}
        {{{0}}}        
        '''.format(col_fmt)
    if not title is None:
        header_s = header_s + r'''
        \caption{{{0}}} \\
        '''.format(title)
    s_line1 = s.partition('\n')[0]
    p = re.compile(r'([-a-zA-Z0-9_\s]+)', re.I)
    match = re.findall(p, s_line1)
    s_line1 = ''
    for num, item in enumerate(match):
        tmp = r'\multicolumn{{1}}{{c}}{{\textbf{{{0}}}}}'.format(item.strip().capitalize())
        if num != len(match) - 1:
            tmp = tmp + ' & '
        else:
            tmp = tmp + ' \\\\'
        s_line1 = s_line1 + tmp
    header_s = header_s + r'''
    \toprule
    {0}
    \midrule
    \endfirsthead
    \multicolumn{{{1}}}{{c}}{{\textbf{{\tablename\ \thetable{{}}:}} (Continued)}} \\
    \midrule
    {0}
    \midrule
    \endhead
    \midrule
    \multicolumn{{{1}}}{{>{{\kern-\tabcolsep}}r<{{\kern-\tabcolsep}}}}{{(\textit{{Continued on next page}})}} \\
    \endfoot    
    \bottomrule
    \endlastfoot
    '''.format(s_line1, t[1])
    s_other_lines = '\n'.join(s.partition('\n')[1:])
    s = header_s + '\n' + s_other_lines + r'''
    \end{longtable}
    '''
    if landscape is True:
        s = s + r'''
        \end{landscape}
        \end{center}
        }
        '''
    else:
        s = s + r'''
        \end{center}
        }
        '''
    s = '\n'.join(x.strip() for x in s.partition('\n'))
    return s

def replace_names(s: str, d: dict):
    '''
    Replace variable names
    @param s: string
    @param d: dictionary
    @return: s
    '''
    for i, j in d.items():
        s = re.sub(r'(\b){0}(\b)'.format(i), r'\g<1>{0}\g<2>'.format(j), s, flags=re.I | re.U)
    return s


def reformat_header(t: tuple) -> tuple:
    '''
    Reformat longtable headers
    @param t: tuple (string, number of cols)
    @return:
    '''
    s = t[0]
    length = t[1]
    p = re.compile(r'\\endlastfoot[\s\n]+\\midrule', re.M)
    s = p.sub(r'\\endlastfoot', s)
    p = re.compile(r'%-{5}start header[\S\s]+%-{5}end header', re.M)
    res = p.search(s)
    if res is not None:
        header_s = res.group(0)
        p = re.compile(r'\\toprule[\s\n]+\\endfirsthead', re.M)
        res = p.search(s)
        if res is not None:
            s = s[:res.start()] + r'''
            \toprule
            {0}
            \endfirsthead
            '''.format(header_s) + s[res.end():]
    t = (s, length)
    return t
